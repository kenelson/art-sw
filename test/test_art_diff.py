#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""TBD."""

import logging
import os
import sys

if sys.version_info < (3, 0, 0):
    sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python/ART/python2.7/site-packages'))
else:
    sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python/ART/python3.6/site-packages'))

from docopt import docopt  # noqa: E402

from ART.art_testcase import ArtTestCase  # noqa: E402
from ART.art_misc import set_log_level  # noqa: E402


class TestArt(ArtTestCase):
    """TBD."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(TestArt, self).__init__(*args, **kwargs)

        # correct location where we would have found art.py
        sys.argv[0] = os.path.join(self.art_directory, 'art.py')

        # to find art.py script and its doc
        sys.path.append(self.art_directory)

        self.cwd = os.getcwd()

    def setUp(self):
        """Setting up Local Tests."""
        set_log_level(logging.DEBUG)
        os.chdir(self.cwd)
        self.test_dir = os.path.join(self.test_directory, 'data_art_diff')
        self.val = '2019-08-30T2140'
        self.ref = '2019-08-16T2137'
        self.package = "Tier0ChainTests"
        self.test = "test_q220"
        self.txt = 'test.txt'
        self.file = "myAOD.pool.root"

    def test_art_diff_no_files(self):
        """Test art diff no-files command."""
        art_diff = __import__('art-diff', fromlist=['__doc__', 'ArtDiff'])
        # two dirs with no direct files or tars
        test_dir = os.path.join(self.test_dir, 'tars')
        ref_dir = os.path.join(self.test_dir, 'files')
        with self.assertRaises(SystemExit) as cm:
            arguments = docopt(art_diff.__doc__, argv=[test_dir, ref_dir])
            exit(art_diff.ArtDiff().parse_and_run(arguments))
        self.assertEqual(cm.exception.code, -1)

    def art_diff_txt_no_ref(self, subdir):
        """Test art diff file command."""
        art_diff = __import__('art-diff', fromlist=['__doc__', 'ArtDiff'])

        val_test = os.path.join(self.test_dir, subdir, self.val, self.package, self.test)
        arguments = docopt(art_diff.__doc__, argv=['--diff-type=diff-txt', '--file=' + self.txt, val_test, 'non-existing-path'])
        exit(art_diff.ArtDiff().parse_and_run(arguments))

    def test_art_diff_txt_no_ref_files(self):
        """Test art diff test-test-files command."""
        with self.assertRaises(SystemExit) as cm:
            self.art_diff_txt_no_ref("files")
        self.assertEqual(cm.exception.code, -1)

    def test_art_diff_txt_no_ref_tars(self):
        """Test art diff test-test-tars command."""
        with self.assertRaises(SystemExit) as cm:
            self.art_diff_txt_no_ref("tars")
        self.assertEqual(cm.exception.code, -1)

    def art_diff_txt(self, subdir):
        """Test art diff file command."""
        art_diff = __import__('art-diff', fromlist=['__doc__', 'ArtDiff'])
        val_test = os.path.join(self.test_dir, subdir, self.val, self.package, self.test)
        ref_test = os.path.join(self.test_dir, subdir, self.val, self.package, self.test)
        arguments = docopt(art_diff.__doc__, argv=['--diff-type=diff-txt', '--file=' + self.txt, val_test, ref_test])
        exit(art_diff.ArtDiff().parse_and_run(arguments))

    def test_art_diff_txt_files(self):
        """Test art diff test-test-files command."""
        with self.assertRaises(SystemExit) as cm:
            self.art_diff_txt("files")
        self.assertEqual(cm.exception.code, 0)

    def test_art_diff_txt_tars(self):
        """Test art diff test-test-tars command."""
        with self.assertRaises(SystemExit) as cm:
            self.art_diff_txt("tars")
        self.assertEqual(cm.exception.code, 0)

    def art_diff_file_path(self, subdir):
        """Test art diff file-path command."""
        art_diff = __import__('art-diff', fromlist=['__doc__', 'ArtDiff'])
        val_file = os.path.join(self.test_dir, subdir, self.val, self.package, self.test, self.file)
        ref_test = os.path.join(self.test_dir, subdir, self.val, self.package, self.test)
        arguments = docopt(art_diff.__doc__, argv=[val_file, ref_test])
        exit(art_diff.ArtDiff().parse_and_run(arguments))

    def test_art_diff_file_path_files(self):
        """Test art diff test-test-files command."""
        with self.assertRaises(SystemExit) as cm:
            self.art_diff_file_path("files")
        self.assertEqual(cm.exception.code, -1)

    def test_art_diff_file_path_tars(self):
        """Test art diff test-test-tars command."""
        with self.assertRaises(SystemExit) as cm:
            self.art_diff_file_path("tars")
        self.assertEqual(cm.exception.code, -1)

    def art_diff_path_file(self, subdir):
        """Test art diff path-file command."""
        art_diff = __import__('art-diff', fromlist=['__doc__', 'ArtDiff'])
        val_test = os.path.join(self.test_dir, subdir, self.val, self.package, self.test)
        ref_file = os.path.join(self.test_dir, subdir, self.val, self.package, self.test, self.file)
        arguments = docopt(art_diff.__doc__, argv=[val_test, ref_file])
        exit(art_diff.ArtDiff().parse_and_run(arguments))

    def test_art_diff_path_file_files(self):
        """Test art diff test-test-files command."""
        with self.assertRaises(SystemExit) as cm:
            self.art_diff_path_file("files")
        self.assertEqual(cm.exception.code, -1)

    def test_art_diff_path_file_tars(self):
        """Test art diff test-test-tars command."""
        with self.assertRaises(SystemExit) as cm:
            self.art_diff_path_file("tars")
        self.assertEqual(cm.exception.code, -1)

    def art_diff_test_test(self, subdir):
        """Test art diff test-test command."""
        art_diff = __import__('art-diff', fromlist=['__doc__', 'ArtDiff'])
        test_file = os.path.join(self.test_dir, subdir, self.val, self.package, self.test)
        ref_file = os.path.join(self.test_dir, subdir, self.ref, self.package, self.test)
        arguments = docopt(art_diff.__doc__, argv=[test_file, ref_file])
        exit(art_diff.ArtDiff().parse_and_run(arguments))

    def test_art_diff_test_test_files(self):
        """Test art diff test-test-files command."""
        with self.assertRaises(SystemExit) as cm:
            self.art_diff_test_test("files")
        self.assertEqual(cm.exception.code, 0)

    def test_art_diff_test_test_tars(self):
        """Test art diff test-test-tars command."""
        with self.assertRaises(SystemExit) as cm:
            self.art_diff_test_test("tars")
        self.assertEqual(cm.exception.code, 0)

    def art_diff_file_file(self, subdir):
        """Test art diff file-file command."""
        art_diff = __import__('art-diff', fromlist=['__doc__', 'ArtDiff'])
        test_file = os.path.join(self.test_dir, subdir, self.val, self.package, self.test, self.file)
        ref_file = os.path.join(self.test_dir, subdir, self.ref, self.package, self.test, self.file)
        arguments = docopt(art_diff.__doc__, argv=[test_file, ref_file])
        exit(art_diff.ArtDiff().parse_and_run(arguments))

    def test_art_diff_file_file_files(self):
        """Test art diff file-file-files command."""
        with self.assertRaises(SystemExit) as cm:
            self.art_diff_file_file("files")
        self.assertEqual(cm.exception.code, 0)

    def test_art_diff_file_file_tars(self):
        """Test art diff file-file-tars command."""
        with self.assertRaises(SystemExit) as cm:
            self.art_diff_file_file("tars")
        self.assertEqual(cm.exception.code, 0)

    def art_diff_path_path(self, subdir):
        """Test art diff path-path command."""
        art_diff = __import__('art-diff', fromlist=['__doc__', 'ArtDiff'])
        test_dir = os.path.join(self.test_dir, subdir, self.val, self.package)
        ref_dir = os.path.join(self.test_dir, subdir, self.ref, self.package)
        arguments = docopt(art_diff.__doc__, argv=[test_dir, ref_dir])
        exit(art_diff.ArtDiff().parse_and_run(arguments))

    def test_art_diff_path_path_files(self):
        """Test art diff path-path-files command."""
        with self.assertRaises(SystemExit) as cm:
            self.art_diff_path_path("files")
        self.assertEqual(cm.exception.code, 0)

    def test_art_diff_path_path_tars(self):
        """Test art diff path-path-tars command."""
        with self.assertRaises(SystemExit) as cm:
            self.art_diff_path_path("tars")
        self.assertEqual(cm.exception.code, 0)
