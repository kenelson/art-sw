# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#export AtlasBuildBranch=21.0
#export AtlasProject=Athena
#export Athena_PLATFORM=x86_64-slc6-gcc62-opt
#export AtlasBuildStamp=latest

export AtlasBuildBranch=master
export AtlasProject=Athena
export Athena_PLATFORM=x86_64-centos7-gcc8-opt
export AtlasBuildStamp=2020-06-05T2140
