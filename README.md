<!-- Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration -->
# ART

ART - ATLAS Nightly Release Tester script
Author: Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>


# Triggering the submission

Either the nightly tests are triggered from the actual build, or they are triggered
from the commandline.

Steps for triggering from the actual build:

1. Nightly build is done and is available on cvmfs
2. gitlab-ci triggered using art-trigger.py with arguments NIGHTLY_RELEASE, PROJECT, PLATFORM, and NIGHTLY_TAG
3. Wait 30 min (for the cvmfs distribution on grid) before submitting ART jobs with "art.py submit"

To start from the commandline:

* art.py submit is called with at least arguments NIGHTLY_RELEASE, PROJECT, PLATFORM, and NIGHTLY_TAG

# art submit

Steps to be executed:

1. Find which packages are included/excluded for NIGHTLY_RELEASE, PROJECT, PLATFORM, and NIGHTLY_TAG
2. Find all the ART scripts for a particular NIGHTLY_RELEASE, PROJECT, PLATFORM, and NIGHTLY_TAG which are to be included
3. Create a temp directory with proper source/build/run subdirs
4. Create a wrapper script including all shell scripts
5. Create a submit script with proper setup for atlas, panda with final pathena command, referring to wrapper
6. Run the submit script
7. Return parameter should be some ID, with which we can make a directory to retrieve the results
8. Make that directory in the output area
9. Download and copy the results and outputs (if requested) to the eos output directory


# External packages (copied)

- docopt: https://github.com/docopt/docopt
- docopt_dispatch: https://github.com/keleshev/docopt-dispatch
