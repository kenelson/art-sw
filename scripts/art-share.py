#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""
ART  - ATLAS Release Tester - Share.

Usage:
  art-share.py [options] <data_directory>

Options:
  -h --help               Show this screen
  --version               Show version
  -q --quiet              Show less information, only warnings and errors
  --write                 Write to directory
  -v --verbose            Show more information, debug level
  --delete-after=<days>   Permanently remove obsolete ART files after this many days [default: 7]
  --max-file-size=<size>  Maximum allowed file size in MB [default: 100]

Arguments:
  data_directory          directory to scan for shared files

"""
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

try:
    import scandir as scan
except ImportError:
    import os as scan

import datetime
import fnmatch
import getpass
import hashlib
import logging
import os
import sys
import time

if sys.version_info < (3, 0, 0):
    sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python/ART/python2.7/site-packages'))
else:
    sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python/ART/python3.6/site-packages'))

from builtins import object  # noqa: E402
from past.utils import old_div  # noqa: E402

from docopt import docopt  # noqa: E402

MODULE = "art.share"


class TrashCan(object):
    def __init__(self, trash_dir, delete_after, log):
        if not os.path.exists(trash_dir):
            try:
                os.makedirs(trash_dir)
            except OSError as err:
                log.exception("Unable to make trash directory %s", trash_dir)
                raise err

        self.trash_dir = trash_dir
        self.delete_days = int(delete_after)
        self.log = logger
        log.info("Trash can location: %s, has %d items in it", self.trash_dir, len(self._trash_contents()))

    def put(self, art_files):
        """Put the given files in to the trash."""
        for af in art_files:
            trash_file = os.path.join(self.trash_dir, os.path.basename(af))
            self.log.info("Move to trash: %s", af)
            try:
                os.rename(af, trash_file)
            except OSError as err:
                self.log.error("Error putting %s in trash", af)
                raise err

    def _trash_contents(self):
        items = os.listdir(self.trash_dir)
        art_files = (f for f in items if fnmatch.fnmatch(f, "*.art"))
        return [os.path.join(self.trash_dir, f) for f in art_files]

    def empty(self):
        """Remove all trash older than delete_days."""
        art_files = self._trash_contents()
        delete_epoch = int(time.time()) - (24 * 3600 * self.delete_days)
        for af in art_files:
            af = os.path.join(self.trash_dir, af)
            create_time = os.path.getctime(af)
            if create_time < delete_epoch:
                # delete
                self.log.info("Remove trash file %s (put in trash %s)", af, time.ctime(create_time))
                try:
                    os.remove(af)
                except OSError as err:
                    self.log.exception("Error removing trash file %s", af)
                    raise err
        self.log.info("Trash can has %d items in it", len(self._trash_contents()))


class NoOpTrashCan(object):
    def __init__(self, trash_dir, *args, **kws):
        self.trash_dir = trash_dir

    def noOp(self, *args, **kws):
        pass

    def __getattr__(self, _):
        return self.noOp


class Results(object):
    def __init__(self):
        self.broken_links = {}
        self.strange_links = {}
        self.ok_links = {}
        self.known_files = {}
        self.new_files = {}
        self.big_files = {}
        self.empty_files = []
        self.bad_dir_perms = {}
        self.bad_file_perms = {}
        self.trashed_files = []

    def report(self, verbose=False):
        log = logging.getLogger(MODULE)

        if not verbose:
            log.info("--- SUMMARY ------------------------------------------")
            log.info('Normal links found:   %d', len(self.ok_links))
            log.info('Broken links found:   %d', len(self.broken_links))
            log.info('Strange links found:  %d', len(self.strange_links))
            log.info('New files found:      %d', len(self.new_files))
            log.info('Empty files found:    %d', len(self.empty_files))
            log.info('Large files found:    %d', len(self.big_files))
            log.info('Known files found:    %d', len(self.known_files))
            log.info('Dirs with bad perms:  %d', len(self.bad_dir_perms))
            log.info('Files with bad perms: %d', len(self.bad_file_perms))
            log.info('Files put in trash:   %d', len(self.trashed_files))
            log.info('------------------------------------------------------')
            return

        if self.broken_links:
            log.debug("--- BROKEN LINKS ------------------------------------")
            for i, (src, tgt) in enumerate(self.broken_links.items(), 1):
                log.debug("[%d] %s --> %s", i, src, tgt)

        if self.strange_links:
            log.info("--- STRANGE LINKS ------------------------------------")
            for i, (src, tgt) in enumerate(self.strange_links.items(), 1):
                log.info("[%d] %s --> %s", i, src, tgt)

        log.debug("--- NORMAL LINKS ------------------------------------")
        for i, (src, tgt) in enumerate(self.ok_links.items(), 1):
            log.debug("[%d] %s (perm=%s) --> %s (perm=%s)", i, src, tgt['perms_link'], tgt['tgt'], tgt['perms_tgt'])

        if self.empty_files:
            log.debug("--- EMPTY FILES -------------------------------------")
            for i, ef in enumerate(self.empty_files, 1):
                log.debug("[%d] %s", i, ef)

        if self.big_files:
            log.info("--- BIG FILES ----------------------------------------")
            for i, (path, size) in enumerate(self.big_files.items(), 1):
                log.info("[%d] %s: %dMB", i, path, size)

        if self.known_files:
            log.info("--- KNOWN FILES --------------------------------------")
            for i, (src, tgt) in enumerate(self.known_files.items(), 1):
                log.info("[%d] %s --> %s", i, src, tgt)

        if self.new_files:
            log.info("--- NEW FILES ----------------------------------------")
            for i, (src, info) in enumerate(self.new_files.items(), 1):
                log.info("[%d] %s (size=%dMB, tgt=%s)", i, src, info['size'], info['tgt'])

        if self.bad_dir_perms:
            log.warning("--- DIRECTORIES WITH BAD PERMISSIONS --------------")
            for i, (path, perm) in enumerate(self.bad_dir_perms.items(), 1):
                log.warning("[%d] %s: %s", i, perm, path)

        if self.bad_file_perms:
            log.warning("--- FILES WITH BAD PERMISSIONS --------------------")
            for i, (path, perm) in enumerate(self.bad_file_perms.items(), 1):
                log.warning("[%d] %s: %s", i, perm, path)

        if self.trashed_files:
            log.warning("--- ART FILES TO TRASH ----------------------------")
            for i, path in enumerate(self.trashed_files, 1):
                log.warning("[%s] %s", i, path)


class ArtShare(object):
    """Class for copying input files.

    Files are copies to the .art area under their SHA1 sum. The orignal file is replaced
    by a symbolic link. Any duplicates will result in the same SHA1 sum and thus just
    be replaced by their symbiolic link. Removing a file results in removing a link.
    If the .art directory has files without links pointing to them, these files are also
    removed.
    """

    def __init__(self, data_directory, write=False, delete_trash_after=7, max_file_size=100):
        """Constructor of ArtShare."""
        print("INFO: art-share", __version__, "executed by", getpass.getuser(), "on", datetime.datetime.today())
        log = logging.getLogger(MODULE)
        if not os.path.isdir(data_directory):
            log.critical("data_directory does not exist: %s", data_directory)
            sys.exit(1)

        self.write = write
        self.max_file_size = max_file_size

        self.data_directory = data_directory
        self.art_directory = os.path.join(self.data_directory, '.art')
        self.extension = '.art'

        trash_dir = os.path.join(self.data_directory, '.trash')
        trashCan = TrashCan if self.write else NoOpTrashCan
        self.trash = trashCan(trash_dir, delete_trash_after, log)

        self.existing_sha1 = set([os.path.splitext(f)[0] for f in os.listdir(self.art_directory) if f.endswith('.art')])
        self.found_sha1 = set()

        self.results = Results()

        if not self.write:
            log.info("This is a dry run, use --write to actually change the EOS file system")
        else:
            if not os.access(self.data_directory, os.W_OK | os.X_OK):
                log.critical("You do not have write permission in %s", self.data_directory)
                sys.exit(1)

            log.warning('***** CHANGING EOS FILESYSTEM *****')

        try:
            self.share()
        except OSError as err:
            log.exception("Caught OSError (%s), filename: %s", os.strerror(err.errno), err.filename)
            sys.exit(1)

        try:
            self.trash.empty()
        except OSError:
            pass

    def calculate_sha1(self, path):
        """Calculate SHA1 from file on path."""
        BUF_SIZE = 65536
        sha1 = hashlib.sha1()
        with open(path, 'rb') as f:
            while True:
                data = f.read(BUF_SIZE)
                if not data:
                    break
                sha1.update(data)
        return sha1.hexdigest()

    def make_art_directory(self):
        log = logging.getLogger(MODULE)
        if not os.path.isdir(self.art_directory):
            msg = "would create" if not self.write else "creating"
            log.info("ART ROOT dir inexistant, %s: %s", msg, self.art_directory)
            if self.write:
                try:
                    os.makedirs(self.art_directory)
                except IOError:
                    log.exception("Failed to create ART root dir: %s", self.art_directory)
                    sys.exit(1)

    def _gather_data(self):
        """Walk the tree rooted at the data directory."""

        def handle_walk_err(err):
            log = logging.getLogger(MODULE)
            log.error("File tree walk error was raised")
            raise err

        # Set of ART files (sha1) that are linked to by something
        # At the end of the tree walk, the difference between this and the existing ART files
        # is the list of files that are not linked to, and can be trashed

        for root, dirs, files in scan.walk(self.data_directory, onerror=handle_walk_err):
            for d in dirs:
                d = os.path.join(root, d)
                self._handle_dir(d)

            if root == self.art_directory or root == self.trash.trash_dir:
                continue

            for f in files:
                path = os.path.join(root, f)
                handle_fn = self._handle_link if os.path.islink(path) else self._handle_file
                handle_fn(path)

        trash_sha1 = self.existing_sha1 - self.found_sha1
        to_trash = [os.path.join(self.art_directory, sha1 + self.extension) for sha1 in trash_sha1]
        self.results.trashed_files.extend(to_trash)

    def _handle_dir(self, dir_path):
        perm = oct(os.stat(dir_path).st_mode)[-3:]
        if '0' in perm:
            self.results.bad_dir_perms[dir_path] = perm

    def _handle_link(self, link_path):
        tgt = os.path.realpath(link_path)

        if not os.path.exists(tgt):
            self.results.broken_links[link_path] = tgt
            return

        if not (os.path.dirname(tgt) == self.art_directory):
            self.results.strange_links[link_path] = tgt
            return

        tgt_perms = oct(os.stat(tgt).st_mode)[-3:]
        link_perms = oct(os.lstat(link_path).st_mode)[-3:]

        sha1 = os.path.splitext(os.path.basename(tgt))[0]

        self.results.ok_links[link_path] = {
            "tgt": tgt,
            "perms_link": link_perms,
            "perms_tgt": tgt_perms,
            "sha1": sha1
        }

        if '0' in tgt_perms:
            self.results.bad_file_perms[tgt] = tgt_perms

        self.found_sha1.add(sha1)

    def _handle_file(self, path):
        byte_size = os.path.getsize(path)
        if byte_size <= 0:
            self.results.empty_files.append(path)
            return

        megabyte_size = old_div(old_div(byte_size, 1024), 1024)
        if megabyte_size >= self.max_file_size:
            self.results.big_files[path] = megabyte_size
            return

        sha1 = self.calculate_sha1(path)

        art_path = os.path.join(self.art_directory, sha1 + self.extension)
        art_relpath = os.path.relpath(art_path, os.path.dirname(path))

        if sha1 not in self.existing_sha1:
            self.results.new_files[path] = {
                "tgt": art_path,
                "tgt_rel": art_relpath,
                "size": megabyte_size,
                "sha1": sha1
            }
        else:
            # Removing a file whose SHA1 is already known about.
            # This means either:
            # a) the link at this location was removed and the original file put back
            # b) the same file was put in a new location/renamed
            self.results.known_files[path] = art_relpath
            self.found_sha1.add(sha1)

    def rename(self, old, new):
        log = logging.getLogger(MODULE)
        log.info("Moving file %s to %s", old, new)
        try:
            os.rename(old, new)
        except OSError as err:
            log.error("Error moving file %s to %s", old, new)
            raise err

    def symlink(self, src, tgt):
        log = logging.getLogger(MODULE)
        log.info("Symlinking %s to %s", src, tgt)
        try:
            os.symlink(src, tgt)
        except OSError as err:
            log.error("Error symlinking %s to %s", tgt, src)
            raise err

    def remove(self, path):
        log = logging.getLogger(MODULE)
        log.info("Removing known file %s", path)
        try:
            os.remove(path)
        except OSError as err:
            log.error("Error removing known file %s", path)
            raise err

    def chmod(self, path, old_perm, new_perm):
        log = logging.getLogger(MODULE)
        log.info("Changing permissions on %s from %s to %s", path, old_perm, new_perm)
        try:
            os.chmod(path, new_perm)
        except OSError as err:
            log.error("Failed to chmod on %s from %s to %s", path, old_perm, new_perm)
            raise err

    def share(self):
        """Share the files by copying."""
        self.make_art_directory()
        self._gather_data()
        self.results.report(verbose=True)

        if self.write:
            for src, info in self.results.new_files.items():
                tgt_path = os.path.join(self.art_directory, info['sha1'] + self.extension)
                self.rename(src, tgt_path)
                self.symlink(info['tgt_rel'], src)

            for src, tgt in self.results.known_files.items():
                self.remove(src)
                self.symlink(tgt, src)

            # for path, perm in self.results.bad_dir_perms.items():
            # self.chmod(path, perm, 0o0644)

            self.trash.put(self.results.trashed_files)

        # summarise
        self.results.report(verbose=False)


if __name__ == '__main__':  # pragma: no cover
    if sys.version_info < (2, 7, 0):
        sys.stderr.write("You need python 2.7 or later to run this script\n")
        sys.exit(1)

    # NOTE: import should be here, to keep the order of the decorators (module first, art last and unused)
    from version import __version__
    args = docopt(__doc__, version=os.path.splitext(os.path.basename(__file__))[0] + ' ' + __version__)

    level = logging.DEBUG if args['--verbose'] else logging.WARN if args['--quiet'] else logging.INFO
    # Goes to stderr by default
    logging.basicConfig(
        format='%(levelname)s %(asctime)s %(message)s',
        datefmt='%H:%M:%S',
        level=level
    )
    logger = logging.getLogger('art')
    ArtShare(args['<data_directory>'], args['--write'], args['--delete-after'], args['--max-file-size'])
