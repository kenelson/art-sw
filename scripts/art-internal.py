#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""
ART-internal - ATLAS Release Tester (internal command).

Usage:
  art-internal.py grid batch  [-v -q -n --run-all-tests --no-build=<nightly_tag> --max-jobs=<N>] <script_directory> <sequence_tag> <package> <outfile> <inform_panda> <job_type> <job_index>
  art-internal.py grid single [-v -q --in=<in_file> --env-keys=<keys> --env-values=<values> --secondary-dss-keys=<keys> --secondary-dss-values=<values> -n --run-all-tests --no-build=<nightly_tag> --max-jobs=<N>] <script_directory> <sequence_tag> <package> <outfile> <inform_panda> <job_name>

Options:
  -h --help                         Show this screen.
  --in=<in_file>                    Normally percentage IN
  --max-jobs=<N>                    Maximum number of concurrent jobs to run [default: 0]
  --env-keys=<keys>                 Comma separated list of keys of key-value pairs to end up in the environment for the test script(ex: RANDOM1, SKIPEVENTS)
  --env-values=<values>             Comma separated list of values of key-value pairs (ex: %RNDM:100, %SKIPEVENTS)
  -n --no-action                    No real submit will be done
  --no-build=<nightly_tag>          No build was created, use 'latest', nightly_tag used for display
  -q --quiet                        Show less information, only warnings and errors
  --run-all-tests                   Runs all tests, ignoring art-include
  --secondary-dss-keys=<keys>       Comma separated list of logical names (ex: HI1, HI2)
  --secondary-dss-values=<values>   Comma separated list of secondary DSS files (ex: %HI1, %HI2)
  -v --verbose                      Show more information, debug level
  --version                         Show version.

Arguments:
  inform_panda                      Inform Big Panda about job
  job_index                         Index of the test inside the package
  job_name                          Index of the test (batch), or its name (single)
  job_type                          Type of job (e.g. grid, ci, local)
  outfile                           Tar filename used for the output of the job
  package                           Package of the test (e.g. Tier0ChainTests)
  script_directory                  Directory containing the package(s) with tests
  sequence_tag                      Sequence tag (e.g. 0 or PIPELINE_ID)
  submit_directory                  Temporary directory with all files for submission

Environment:
  AtlasBuildBranch                  Name of the nightly release (e.g. 21.0)
  AtlasProject                      Name of the project (e.g. Athena)
  <AtlasProject>_PLATFORM           Platform (e.g. x86_64-slc6-gcc62-opt)
  AtlasBuildStamp                   Nightly tag (e.g. 2017-02-26T2119)
"""
from __future__ import print_function
from __future__ import unicode_literals

import logging
import os
import sys

if sys.version_info < (3, 0, 0):
    sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python/ART/python2.7/site-packages'))
else:
    sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python/ART/python3.6/site-packages'))

from docopt import docopt  # noqa: E402

# add ../python to library search path
sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python'))

import ART  # noqa: E402

from ART import art_internal_commands  # noqa: E402

MODULE = "art.internal"


def _get_art_directory():
    """Retrieve the directory where art.py is installed."""
    artpath = __file__

    # Turn pyc files into py files if we can
    if artpath.endswith('.pyc') and os.path.exists(artpath[:-1]):
        artpath = artpath[:-1]

    return os.path.dirname(os.path.realpath(artpath))


def dispatch(doc, argv=None, help_option=True, version=None, options_first=False):
    arguments = docopt(doc, argv=argv, help=help_option, version=version, options_first=options_first)
    x = [k for (k, v) in arguments.items() if (v and (not k.startswith('-') and not k.startswith('<')))]
    command = x[0].replace('-', '_') if x else None
    # NOTE: if command 'single/batch' is found, we should switch with the sub-command, as 'single/batch' is the sub-command
    if command == 'batch' or command == 'single':
        second = x[1].replace('-', '_') if len(x) > 1 else None
        command = command if second is None else second
    f = getattr(art_internal_commands, "{}".format(command), art_internal_commands.fail)
    return f(arguments, _get_art_directory())


if __name__ == '__main__':  # pragma: no cover
    if sys.version_info < (2, 7, 0):
        sys.stderr.write("You need python 2.7 or later to run this script\n")
        exit(1)

    # NOTE: import should be here, to keep the order of the decorators (module first, art last and unused)
    from version import __version__
    logging.basicConfig()
    log = logging.getLogger(MODULE)
    log.setLevel(logging.INFO)
    log.info("ART      %s", os.path.dirname(os.path.realpath(__file__)))
    log.info("ART_PATH %s", _get_art_directory())
    log.info("ART_LIB  %s", os.path.dirname(os.path.dirname(ART.__file__)))
    dispatch(__doc__, argv=sys.argv[1:], version=os.path.splitext(os.path.basename(__file__))[0] + ' ' + __version__)
