#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""
ART - ATLAS Release Tester.

You need to setup for an ATLAS release before using ART.

Usage:
  art.py run             [-v -q --type=<T> --max-jobs=<N> --ci --run-all-tests --timeout=<S> --copy=<dir>] <script_directory> <sequence_tag> [<test_names>...]
  art.py grid            [-v -q --type=<T> --max-jobs=<N> -n --run-all-tests --no-build=<nightly_tag>] <script_directory> <sequence_tag>
  art.py submit          [-v -q --type=<T> --max-jobs=<N> --config=<file> -n --run-all-tests --no-build=<nightly_tag> --no-copy] <sequence_tag> [<packages>...]
  art.py copy            [-v -q --user=<user> --dst=<dir> --unpack --tmp=<dir> --seq=<N> --keep-tmp] <indexed_package>
  art.py validate        [-v -q] [<script_directory>]
  art.py included        [-v -q --type=<T> --test-type=<TT>] [<script_directory> [<packages>...]]
  art.py download        [-v -q --max-refs=<N> --user=<user> --dst=<dir> --nightly-release=<release> --project=<project> --platform=<platform>] <package> <test_name>
  art.py compare grid    [-v -q --max-refs=<N> --user=<user> --entries=<entries> --file=<pattern>... --txt-file=<file>... --mode=<mode> --no-diff-meta --diff-meta --diff-pool --diff-root --ignore-exit-code=<diff>...  --no-out --order-trees --exclude-var=<var>... --ignore-leave=<pattern>... --exact-branches --meta-mode=<mode>] <package> <test_name>
  art.py compare ref     [-v -q --entries=<entries> --file=<pattern>... --txt-file=<file>... --mode=<mode> --no-diff-meta --diff-meta --diff-pool --diff-root --ignore-exit-code=<diff>... --no-out --order-trees --exclude-var=<var>... --ignore-leave=<pattern>... --branch-of-interest=<pattern>... --exact-branches --meta-mode=<mode>] <path> <ref_path>
  art.py list grid       [-v -q --user=<user> --json --test-type=<TT>] <package>
  art.py output grid     [-v -q --user=<user>] <package> <test_name>
  art.py configuration   [-v -q --config=<file>] [<package>]
  art.py createpoolfile  [-v -q]
  art.py input-collect   [-v -q --type=<T> --run-all-tests --copy=<dir>] <release>
  art.py input-merge     [-v -q --work-dir=<dir> --ref-file=<file>]

Options:
  --branch-of-interest=<pattern>...     Branch of interest for diff-root
  --ci                                  Run Continuous Integration tests only (using env: AtlasBuildBranch)
  --config=<file>                       Use specific config file [default: art-configuration.yml]
  --copy=<dir>                          Copy results to <dir>
  --diff-meta                           Do meta-diff comparison (runs always if POOL or ROOT are compared)
  --diff-pool                           Do comparison with POOL
  --diff-root                           Do comparison with ROOT (none specified means POOL and ROOT are compared)
  --dst=<dir>                           Destination directory for downloaded files
  --entries=<entries>                   Number of entries to compare [default: 10]
  --exact-branches                      Use exact branches
  --exclude-var=<var>...                Exclude var for comparison with diff-meta
  --file=<pattern>...                   Compare the following file patterns for diff-root [default: *AOD*.pool.root *ESD*.pool.root *HITS*.pool.root *RDO*.pool.root *TAG*.root]
  -h --help                             Show this screen
  --ignore-exit-code=<diff>...          Ignore exit code for diff. One of: diff-meta, diff-pool, diff-root, diff-txt
  --ignore-leave=<pattern>...           Ignore leave for diff-root
  --json                                Output in json format
  --keep-tmp                            Keep temporary directory while copying
  --max-jobs=<N>                        Maximum number of concurrent jobs to run [default: 0]
  --max-refs=<N>                        Maximum number of references to look for valid reference for compare or download [default: 7]
  --meta-mode=<mode>                    Sets the mode for diff-meta [default: full]
  --mode=<mode>                         Sets the mode for diff-root {summary, detailed} [default: detailed]
  --nightly-release=<nightly_release>   Use different nighly_release for download
  -n --no-action                        No real submit will be done
  --no-build=<nightly_tag>              No build was created, use 'latest', nightly_tag used for display
  --no-copy                             Do not wait or make a copy
  --no-diff-meta                        Do not run meta-diff comparison
  --no-out                              Do not write output to file: art-compare.log
  --order-trees                         Order trees for diff-root
  --platform=<platform>                 Use different platform for download
  --project=<project>                   Use different project for download
  -q --quiet                            Show less information, only warnings and errors
  --ref-file=<file>                     Reference file to compare with
  --run-all-tests                       Ignores art-include headers, runs all tests
  --seq=<N>                             Use N as postfix on destination nightly-tag (for retries) [default: 0]
  --test-type=<TT>                      Type of test (e.g. all, batch or single) [default: all]
  --timeout=<S>                         Timeout of test in seconds [default: 0]
  --tmp=<dir>                           Temporary directory for downloaded files and caching of EXT0
  --txt-file=<file>...                  Compare the following txt files
  --type=<T>                            Type of job (e.g. grid, local)
  --unpack                              Unpack downloaded tar files
  --user=<user>                         User to use for RUCIO
  -v --verbose                          Show more information, debug level
  --version                             Show version
  --work-dir=<dir>                      Working directory [default: .]

Sub-commands:
  run               Run jobs from a package on local machines (needs release and grid setup)
  grid              Run jobs from a package on the grid (needs release and grid setup)
  submit            Submit nightly jobs to the grid and informs big panda (NOT for users)
  copy              Copy outputs and logs from RUCIO
  validate          Check headers in tests
  included          Show list of files which will be included for art submit/art grid
  compare           Compare the output of a job
  list              List the jobs of a package
  output            Get the output of a job
  configuration     Show configuration
  createpoolfile    Creates an 'empty' poolfile catalog
  download          Downloads the output of a reference job for comparison
  input-collect     Scan latest test directories for declarations of art-input
  input-merge       Merges all input files and compares to reference file

Arguments:
  indexed_package   Package of the test or indexed package (e.g. MooPerformance.4)
  package           Package of the test (e.g. Tier0ChainTests)
  packages          List of packages (e.g. Tier0ChainTests)
  path              Directory or File to compare
  ref_path          Directory or File to compare to
  release           A 'nightly_release/project/platform' combination, no spaces; platform default = x86_64-slc6-gcc62-opt
  script_directory  Directory containing the package(s) with tests
  sequence_tag      Sequence tag (e.g. 0 or PIPELINE_ID)
  test_name         Name of the test inside the package (e.g. test_q322.sh)
  test_names        Names of the test inside the package (e.g. test_q322.sh test_q222.sh)

Environment:
  AtlasBuildBranch          Name of the nightly release (e.g. 21.0)
  AtlasProject              Name of the project (e.g. Athena)
  <AtlasProject>_PLATFORM   Platform (e.g. x86_64-slc6-gcc62-opt)
  AtlasBuildStamp           Nightly tag (e.g. 2017-02-26T2119)

Tests are called with:
  arguments: PACKAGE TEST_NAME SCRIPT_DIRECTORY TYPE [IN_FILE]
  environment: ArtScriptDirectory, ArtPackage, ArtJobType, ArtJobName, [ArtInFile]
"""
from __future__ import print_function
from __future__ import unicode_literals

import logging
import os
import sys

if sys.version_info < (3, 0, 0):
    sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python/ART/python2.7/site-packages'))
else:
    sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python/ART/python3.6/site-packages'))

# add ../python to library search path
sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python'))

from docopt import docopt  # noqa: E402

from version import __version__  # noqa: E402

import ART  # noqa: E402
from ART import art_commands  # noqa: E402

MODULE = "art"


def _get_art_directory():
    """Retrieve the directory where art.py is installed."""
    artpath = __file__

    # Turn pyc files into py files if we can
    if artpath.endswith('.pyc') and os.path.exists(artpath[:-1]):  # pragma: no cover
        artpath = artpath[:-1]

    return os.path.dirname(os.path.realpath(artpath))


def dispatch(doc, argv=None, help_option=True, version=None, options_first=False):
    """Dispatch the command."""
    arguments = docopt(doc, argv=argv, help=help_option, version=version, options_first=options_first)
    # print(arguments)
    # x = [k for (k, v) in arguments.items() if (v and (not k.startswith('-') and not k.startswith('<')))]
    x = [k for (k, v) in arguments.items() if (v and (not k.startswith('-') and not k.startswith('<')))]
    command = x[0].replace('-', '_') if x else None
    # NOTE: if command 'grid' is found, we should look for a sub-command, as some commands use 'grid' as the sub-command
    if command == 'grid':
        second = x[1].replace('-', '_') if len(x) > 1 else None
        command = command if second is None else second
    f = getattr(art_commands, "{}".format(command), art_commands.fail)
    return f(arguments, _get_art_directory())


if __name__ == '__main__':  # pragma: no cover
    if sys.version_info < (2, 7, 0):
        sys.stderr.write("You need python 2.7 or later to run this script\n")
        exit(1)

    logging.basicConfig()
    logger = logging.getLogger(MODULE)
    logger.setLevel(logging.INFO)
    logger.info("ART      %s", os.path.dirname(os.path.realpath(__file__)))
    logger.info("ART_PATH %s", _get_art_directory())
    logger.info("ART_LIB  %s", os.path.dirname(os.path.dirname(ART.__file__)))
    logger.info("ART_VERSION  %s", __version__)
    print(sys.argv)
    dispatch(__doc__, argv=sys.argv[1:], version=os.path.splitext(os.path.basename(__file__))[0] + ' ' + __version__)
