#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""
ART  - ATLAS Release Tester - Clean.

Usage:
  art-clean.py [-v -q --base-dir=<base_dir> --delete --days=<days> --eos --config=<file> --release --package=<package>] [<nightly_release> <project> [<platform>]]

Options:
  --base-dir=<base_dir>      Start search from basedir [default: /eos/atlas/atlascerngroupdisk/data-art/grid-output]
  --config=<file>            art-configuration to retrieve packages and days [default: art-configuration.yml]
  --delete                   Actually delete the directories to be cleaned
  --days=<days>              Number of nighlies to keep [default: 7]
  --eos                      Use eos commands
  -h --help                  Show this screen
  --package=<package>        Package to be cleaned up, no configuration
  --release                  Clean up full release, no configuration
  -q --quiet                 Show less information, only warnings and errors
  -v --verbose               Show more information, debug level
  --version                  Show version

Arguments:
  nightly_release            Name of the nightly release (e.g. 21.0)
  project                    Project to clean (e.g. Athena)
  platform                   Platform to clean [default: x86_64-slc6-gcc62-opt]

Environment:
  AtlasBuildBranch          Name of the nightly release (e.g. 21.0)
  AtlasProject              Name of the project (e.g. Athena)
  <AtlasProject>_PLATFORM   Platform (e.g. x86_64-slc6-gcc62-opt)
"""

import datetime
import logging
import os
import re
import shutil
import sys

if sys.version_info < (3, 0, 0):
    sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python/ART/python2.7/site-packages'))
else:
    sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python/ART/python3.6/site-packages'))

from builtins import object  # noqa: E402

from docopt import docopt  # noqa: E402

# add ../python to library search path
sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'python'))

from version import __version__  # noqa: E402
import ART  # noqa: E402

from ART.art_misc import get_atlas_env, run_command, split_eos_path  # noqa: E402
from ART.art_configuration import ArtConfiguration  # noqa: E402

MODULE = "art.clean"


class ArtClean(object):
    """Class to cleanup eos area."""

    DEFAULT_EOS_MGM_URL = 'root://eosatlas.cern.ch'

    def __init__(self, arguments):
        """Clean when more than 'days' old."""
        log = logging.getLogger(MODULE)
        self.verbose = arguments['--verbose']
        self.eos = arguments['--eos']
        default_days = int(arguments['--days'])
        self.delete = arguments['--delete']
        nightly_release = arguments['<nightly_release>']
        project = arguments['<project>']
        platform = arguments['<platform>']
        base_dir = arguments['--base-dir']

        # Check for missing arguments
        if nightly_release is None:
            (nightly_release, project, platform, dummy) = get_atlas_env()

        if platform is None:
            platform = 'x86_64-slc6-gcc62-opt'
            log.info("Defaulting to platform %s", platform)

        if self.eos:
            # Test if we have access to kerberos
            (code, _out, err, _command, _start_time, _end_time, _timed_out) = run_command('klist', verbose=self.verbose)
            if code != 0:
                log.critical("%s", err)
                exit(1)

        package = arguments['--package']
        if package is not None:
            self.clean_release(os.path.join(base_dir, nightly_release, project, platform), default_days, package)
            return

        if arguments['--release']:
            self.clean_release(os.path.join(base_dir, nightly_release, project, platform), default_days)
            return

        config_file = arguments['--config']
        config = ArtConfiguration(config_file)
        for package in config.packages():
            copy = config.get(nightly_release, project, platform, package, 'copy', False)
            if copy:
                days = int(config.get(nightly_release, project, platform, package, 'days', default_days))
                base_dir = config.get(nightly_release, project, platform, package, 'dst', base_dir)
                self.clean_release(os.path.join(base_dir, nightly_release, project, platform), days, package)
        return

    def clean_release(self, release, days, package=None):
        """Clean a release dir."""
        log = logging.getLogger(MODULE)
        log.info("Starting to clean up release, keeping %d days, for %s in %s", days, "All" if package is None else package, release)
        now = datetime.datetime.now()
        count = 0
        date = '1970-01-01'
        for entry in reversed(self.listdirs(release)):
            # Matches 2018-12-05T0345
            match = re.match(r"(\d{4}-\d{2}-\d{2})T\d{4}", entry)
            if match:
                tag = os.path.join(release, entry)
                # package_dir = tag for full release cleanup
                package_dir = tag if package is None else os.path.join(tag, package)

                # check if package exists
                if self.isdir(package_dir):
                    # only count entries on different dates and where package actually exist
                    if match.group(1) != date:
                        count += 1
                        date = match.group(1)

                    if count > days:
                        # compare times
                        dir_time = datetime.datetime.strptime(match.group(0), '%Y-%m-%dT%H%M')
                        time_diff = now - dir_time
                        if time_diff.days > days:
                            log.info("- Nightly tag %d days old, removing %s for package %s", time_diff.days, package_dir, package if package is not None else 'All')
                            self.remove_dir(package_dir)
                            if package is not None and self.isempty(tag):
                                log.info("- Nightly tag contains no more packages, removing %s for package %s", tag, package if package is not None else 'All')
                                self.remove_dir(tag)
                        else:
                            log.debug("- Tag within %d days, keeping %s for package %s", days, tag, package if package is not None else 'All')
                    else:
                        log.debug("- Tag within %d entries, keeping %s for package %s", days, tag, package if package is not None else 'All')

        if package is not None and count == 0:
            log.info("No package %s found in any available nightly tag for %s", package, release)

    def listdirs(self, directory):
        """Return list of directories in directory."""
        dirs = []
        if self.isdir(directory):
            for entry in self.listdir(directory):
                path = os.path.join(directory, entry)
                if self.isdir(path):
                    dirs.append(entry)
        return dirs

    def isempty(self, directory):
        """Return true if directory is empty."""
        if self.isdir(directory):
            for _entry in self.listdir(directory):
                return False
            return True

    def listdir(self, directory):
        """Return list of entries in directory."""
        log = logging.getLogger(MODULE)
        if self.eos:
            (eos_mgm_url, directory) = split_eos_path(directory, ArtClean.DEFAULT_EOS_MGM_URL)
            (code, out, err, _command, _start_time, _end_time, _timed_out) = run_command('eos ' + eos_mgm_url + ' ls ' + directory, verbose=self.verbose)
            if code == 0:
                return out.splitlines()
            log.info("eos listdir %d %s", code, err)
            return []
        else:
            return os.listdir(directory)

    def isdir(self, path):
        """Return true is path is directory."""
        log = logging.getLogger(MODULE)
        if self.eos:
            (eos_mgm_url, path) = split_eos_path(path, ArtClean.DEFAULT_EOS_MGM_URL)
            (code, _out, err, _command, _start_time, _end_time, _timed_out) = run_command('eos ' + eos_mgm_url + ' stat -d ' + path, verbose=self.verbose)
            if code == 0:
                return True
            log.debug("eos isdir(%s) %d %s", path, code, err)
            return False
        else:
            return os.path.isdir(path)

    def remove_dir(self, directory):
        """Remove directory and all below."""
        log = logging.getLogger(MODULE)
        if not self.isdir(directory):
            return

        if self.delete:
            if self.eos:
                (eos_mgm_url, directory) = split_eos_path(directory, ArtClean.DEFAULT_EOS_MGM_URL)
                (code, _out, err, _command, _start_time, _end_time, _timed_out) = run_command('eos ' + eos_mgm_url + ' rm -r ' + directory, verbose=self.verbose)
                if code == 0:
                    return True
                log.info("eos rm -r (%s) %d %s", directory, code, err)
            else:
                shutil.rmtree(directory)


def _get_art_directory():
    """Retrieve the directory where art.py is installed."""
    artpath = __file__

    # Turn pyc files into py files if we can
    if artpath.endswith('.pyc') and os.path.exists(artpath[:-1]):  # pragma: no cover
        artpath = artpath[:-1]

    return os.path.dirname(os.path.realpath(artpath))


if __name__ == '__main__':
    if sys.version_info < (2, 7, 0):
        sys.stderr.write("You need python 2.7 or later to run this script\n")
        exit(1)

    # NOTE: import should be here, to keep the order of the decorators (module first, art last and unused)

    logging.basicConfig()
    logger = logging.getLogger('art')
    logger.setLevel(logging.INFO)
    logger.info("ART_PATH %s", _get_art_directory())
    logger.info("ART_LIB  %s", os.path.dirname(os.path.dirname(ART.__file__)))

    args = docopt(__doc__, version=os.path.splitext(os.path.basename(__file__))[0] + ' ' + __version__)
    level = logging.DEBUG if args['--verbose'] else logging.WARN if args['--quiet'] else logging.INFO
    logger.setLevel(level)
    ArtClean(args)
