#!/bin/bash
# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# NOTE do NOT run with /bin/bash -x as the output is too big for gitlab-ci
# arguments:  DID_PATTERN
#
# author : Tulay Cuhadar Donszelmann <tcuhadar@cern.ch>
#
# example: art-rucio-list-dids DID_PATTERN

if [ $# -ne 1 ]; then
    echo 'Usage: art-rucio-list-dids.sh DID_PATTERN'
    exit 1
fi

# remember the original stdout in 9, then redirect stdout to stderr
exec 9>&1 >&2

DID_PATTERN=$1
shift

export ATLAS_LOCAL_ROOT_BASE="${ATLAS_LOCAL_ROOT_BASE:-/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase}"
## shellcheck source=/dev/null
source "${ATLAS_LOCAL_ROOT_BASE}"/user/atlasLocalSetup.sh --quiet

unset ALRB_noGridMW

lsetup -f "rucio -w"
lsetup -f "xrootd 5.1.1"
# lsetup -f xrootd

echo "DID_PATTERN: ${DID_PATTERN}"

# Do not use: rucio delivers warnings as exit code 127
#set -e

# reconnect stdout to the original stdout and close fd 9
exec >&9 9>&-

rucio list-dids --short "${DID_PATTERN}"
