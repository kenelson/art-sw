import os.path

_version_file_ = os.path.join(os.path.dirname(os.path.realpath(__file__)), "art_version.txt.py")


class art_version:
    def __init__(self, major=0, minor=0, patch=0):
        self.major = major
        self.minor = minor
        self.patch = patch

    def update(self, version_str):
        fields = [f.strip() for f in version_str.strip().split('.')]
        if len(fields) == 3:
            try:
                self.major = int(fields[0])
                self.minor = int(fields[1])
                self.patch = int(fields[2])
            except ValueError as ve:
                print("Expected a version A.B.C where A,B,C are ints. Got " + version_str)
                raise ve

    def __bool__(self):
        return not (self.major == 0 and self.minor == 0 and self.patch == 0)

    # Python 2
    __nonzero__ = __bool__

    def __eq__(self, other):
        ok = (self.major == other.major)
        ok = ok and (self.minor == other.minor)
        ok = ok and (self.patch == other.patch)
        return ok

    def __str__(self):
        return str(self.major) + "." + str(self.minor) + "." + str(self.patch)

    def bump_major(self):
        return art_version(self.major + 1, 0, 0)

    def bump_minor(self):
        return art_version(self.major, self.minor + 1, 0)

    def bump_patch(self):
        return art_version(self.major, self.minor, self.patch + 1)


def _read_version_file():
    if not _art_version_:
        with open(_version_file_) as fd:
            # remove the quotes
            v_str = fd.read().strip()[1:-1]
            _art_version_.update(v_str)
    return str(_art_version_)


def _write_version_file():
    if _art_version_:
        with open(_version_file_, 'w') as fd:
            fd.write(str(_art_version_))


_art_version_ = art_version()
__version__ = _read_version_file()
