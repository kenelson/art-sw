#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""Base class for grid and local submits."""
from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

from builtins import str
from builtins import object

try:
    import scandir as scan
except ImportError:  # pragma: no cover
    import os as scan

import calendar
import collections
import fnmatch
import glob
import json
import logging
import os
import re
import sys

from datetime import datetime, timedelta

from .art_configuration import ArtConfiguration
from .art_diff import ArtDiff
from .art_header import ArtHeader
from .art_misc import dict_added, dict_changed, dict_removed, get_release, local_script_directory, split_release, xrdcp

MODULE = "art.base"


class ArtBase(object):
    """Base class for grid and local submits."""

    COMPARE_LOG = 'art-compare.log'

    def __init__(self):
        """Keep arguments."""

    def validate(self, script_directory):
        """Validate all tests in given script_directory."""
        log = logging.getLogger(MODULE)
        log.info("Validating script directory: %s", script_directory)
        directories = self.get_test_directories(script_directory.rstrip("/"))

        found_test = False
        valid = True
        for directory in directories.values():
            files = self.get_files(directory, valid_only=False)
            # print(files)
            for fname in files:
                test_name = os.path.join(directory, fname)
                found_test = True
                valid &= ArtHeader(test_name).validate()

        if not found_test:
            log.warning('No scripts found in %s directory', script_directory)
            return 1

        if not valid:
            log.error('Some scripts have invalid headers or are not executable')
            return 1

        log.info("Scripts in %s directory are valid", script_directory)
        return 0

    def included(self, script_directory, job_type, index_type, packages, branch, project, platform):
        """Return map of art-include values for each test_name found."""
        log = logging.getLogger(MODULE)
        directories = self.get_test_directories(script_directory.rstrip("/"))

        result = {}
        if not packages:
            # search all packages
            for directory in directories.values():
                files = self.get_files(directory, job_type=job_type, index_type=index_type)
                branch = self.adjust_branch(directory, files, branch, project, platform)
                for fname in files:
                    test_name = os.path.join(directory, fname)
                    if self.is_included(test_name, branch, project, platform):
                        result[test_name] = ArtHeader(test_name).get(ArtHeader.ART_INCLUDE)
        else:
            # search list of packages
            for package in packages:
                if package in directories:
                    directory = directories[package]
                    files = self.get_files(directory, job_type=job_type, index_type=index_type, branch=branch, project=project, platform=platform)
                    branch = self.adjust_branch(directory, files, branch, project, platform)
                    for fname in files:
                        test_name = os.path.join(directory, fname)
                        if self.is_included(test_name, branch, project, platform):
                            result[test_name] = ArtHeader(test_name).get(ArtHeader.ART_INCLUDE)
                else:
                    log.warning("Package does not exist: %s", package)
        if len(result) == 0:
            log.warning('No tests found')

        return result

    def input_collect(self, job_type, run_all_tests, copy, release):
        """Collect all art-input."""
        from builtins import open

        log = logging.getLogger(MODULE)
        inp = collections.defaultdict(dict)

        (nightly_release, project, platform) = split_release(release, platform='x86_64-slc6-gcc62-opt')
        script_directory = local_script_directory(None, nightly_release, project, platform)
        log.debug("SCRIPT_DIRECTORY = %s", script_directory)
        if script_directory is None:  # pragma: no cover
            log.critical("Cannot find valid script directory")
            exit(1)

        directories = self.get_test_directories(script_directory.rstrip("/"))
        for directory in directories.values():
            files = self.get_files(directory, nightly_release=nightly_release, project=project, platform=platform, job_type=job_type, run_all_tests=run_all_tests, no_build=False, valid_only=True, run_on=False)
            for fname in files:
                test_name = os.path.join(directory, fname)
                header = ArtHeader(test_name)
                log.debug("Test %s", test_name)
                inds = header.get(ArtHeader.ART_INPUT)
                if inds is not None:
                    n_files = header.get(ArtHeader.ART_INPUT_NFILES)
                    inp[inds]['max_files'] = max(inp.get(inds, {}).get('max_files', 0), n_files)
                    inp[inds][test_name] = n_files
                    log.debug("%s %d", inds, n_files)

        filename = '-'.join((nightly_release, project, platform, 'art-input')) + ".json"
        with open(filename, "w", encoding='UTF-8') as outfile:
            # json.dump(inp, outfile, sort_keys=True, indent=4, encoding='utf-8')
            outfile.write(str(json.dumps(inp, sort_keys=True, indent=4)))

        if copy and copy != '.':
            xrdcp(filename, copy)

        return 0

    def input_merge(self, work_dir, ref_file):
        """TBD."""
        from builtins import open

        inp = collections.defaultdict(dict)

        lst = glob.glob(os.path.join(work_dir, '*-art-input.json'))
        for input_file in lst:
            with open(input_file, "r", encoding='UTF-8') as f:
                file = json.load(f)

                for key in dict_changed(file, inp):
                    for subkey in dict_changed(file[key], inp[key]):
                        inp[key][subkey] = max(inp[key][subkey], file[key][subkey])

                    for subkey in dict_added(file[key], inp[key]):
                        inp[key][subkey] = file[key][subkey]

                for key in dict_added(file, inp):
                    inp[key] = file[key]

        input_file = os.path.join(work_dir, 'art-input.json')
        with open(input_file, "w", encoding='UTF-8') as outfile:
            # json.dump(inp, outfile, sort_keys=True, indent=4, encoding='utf-8')
            outfile.write(str(json.dumps(inp, sort_keys=True, indent=4)))

        return 0 if ref_file is None else self.input_diff(input_file, os.path.join(work_dir, ref_file))

    def input_diff(self, input_file, ref_file):
        """Compare art-input.json with a reference file."""
        from builtins import open

        with open(input_file, "r", encoding='UTF-8') as f:
            inp = json.load(f)

        with open(ref_file, "r", encoding='UTF-8') as f:
            ref = json.load(f)

        result = 0
        for key in dict_added(inp, ref):
            result = 1
            print('A', key, inp[key]['max_files'])

        for key in dict_changed(inp, ref):
            if inp[key]['max_files'] != ref[key]['max_files']:
                result = 1
                print('C', key, ref[key]['max_files'], ' -> ', inp[key]['max_files'])

        for key in dict_removed(inp, ref):
            result = 1
            print('D', key, ref[key]['max_files'])

        if result:
            print('Added (A), Changed (C), Removed (D)')

        return result

    def config(self, package, nightly_release, project, platform, config):
        """Show configuration."""
        log = logging.getLogger(MODULE)
        config = ArtConfiguration(config)
        if package is None:
            log.info("%s", config.packages())
            return 0

        keys = config.keys(nightly_release, project, platform, ArtConfiguration.ALL)
        keys.update(config.keys(nightly_release, project, platform, package))
        for key in keys:
            log.info("'%s'-'%s'", key, config.get(nightly_release, project, platform, package, key))
        return 0

    def update_result_pattern(self, exit_pattern, result_pattern, index, result):
        for name in result_pattern:
            if name not in exit_pattern:
                exit_pattern[name] = ['na'] * 5
            exit_pattern[name][index] = result
        return exit_pattern

    #
    # Default implementations
    #
    def compare_ref(self, path, ref_path, files, txt_files, diff_meta, diff_pool, diff_root, ignore_exit_codes=[], entries=-1, mode='detailed', order_trees=False, exclude_vars=[], ignore_leaves=[], branches_of_interest=[], exact_branches=False, meta_mode=None, no_out=False):
        """TBD."""
        log = logging.getLogger(MODULE)
        log.info("compare_ref called with diff_meta=%s, diff_pool=%s, diff_root=%s", diff_meta, diff_pool, diff_root)
        log.info("see art-compare.log for detailed output of comparison")
        result = 0

        if not no_out:
            temp_stdout = sys.stdout
            file = open(ArtBase.COMPARE_LOG, "a")
            sys.stdout = file

        print('-' * 80)
        print('ART-Compare: Starting...')
        nightly_release = os.environ['AtlasBuildBranch']
        project = os.environ['AtlasProject']
        platform = os.environ[project + '_PLATFORM']
        nightly_tag = os.environ['AtlasBuildStamp']
        package = os.environ.get('ArtPackage', '')
        job_name = os.environ.get('ArtJobName', '')

        print("INFO: ", nightly_release, project, platform, nightly_tag, package, job_name)
        print('-' * 80)

        art_diff = ArtDiff()

        exit_pattern = {}

        if diff_meta:
            # (diff_meta_exit_code, out, err, _command, _start_time, _end_time, _timed_out) = run_command(' '.join(("art-diff.py", "--diff-type=diff-meta", ('--meta-mode=' + meta_mode if meta_mode else ''), (' '.join(('--exclude-var=' + s for s in exclude_vars))), (' '.join(('--file=' + s for s in files))), path, ref_path)))
            (diff_meta_exit_code, diff_meta_exit_pattern) = art_diff.run(path, ref_path,
                                                                         "diff-meta", files,
                                                                         mode=meta_mode, exclude_vars=exclude_vars)
            if diff_meta_exit_code != 0:  # pragma: no cover
                if 'diff-meta' not in ignore_exit_codes:
                    result |= diff_meta_exit_code
            exit_pattern = self.update_result_pattern(exit_pattern, diff_meta_exit_pattern, art_diff.diff_index('diff-meta'), diff_meta_exit_code)

        if diff_pool:
            # (diff_pool_exit_code, out, err, _command, _start_time, _end_time, _timed_out) = run_command(' '.join(("art-diff.py", "--diff-type=diff-pool", (' '.join(('--file=' + s for s in files))), path, ref_path)))
            (diff_pool_exit_code, diff_pool_exit_pattern) = art_diff.run(path, ref_path, "diff-pool", files)
            if diff_pool_exit_code != 0:  # pragma: no cover
                if 'diff-pool' not in ignore_exit_codes:
                    result |= diff_pool_exit_code
            exit_pattern = self.update_result_pattern(exit_pattern, diff_pool_exit_pattern, art_diff.diff_index('diff-pool'), diff_pool_exit_code)

        if diff_root:
            # (diff_root_exit_code, out, err, _command, _start_time, _end_time, _timed_out) = run_command(' '.join(("art-diff.py", "--diff-type=diff-root", "--mode=" + mode, "--entries=" + str(entries), "--order-trees" if order_trees else "", (' '.join(('--file=' + s for s in files))), (' '.join(('--ignore-leave=' + s for s in ignore_leaves))), (' '.join(('--branch-of-interest=' + s for s in branches_of_interest))), "--exact-branches" if exact_branches else "", path, ref_path)))
            (diff_root_exit_code, diff_root_exit_pattern) = art_diff.run(path, ref_path,
                                                                         "diff-root", files,
                                                                         entries=str(entries), mode=mode, order_trees=order_trees,
                                                                         ignore_leaves=ignore_leaves, branches_of_interest=branches_of_interest,
                                                                         exact_branches=exact_branches)
            if diff_root_exit_code != 0:  # pragma: no cover
                if 'diff-root' not in ignore_exit_codes:
                    result |= diff_root_exit_code
            exit_pattern = self.update_result_pattern(exit_pattern, diff_root_exit_pattern, art_diff.diff_index('diff-root'), diff_root_exit_code)

        if txt_files:
            # (txt_files_exit_code, out, err, _command, _start_time, _end_time, _timed_out) = run_command(' '.join(("art-diff.py", "--diff-type=diff-txt", (' '.join(('--file=' + s for s in txt_files))), path, ref_path)))
            (txt_files_exit_code, txt_files_exit_pattern) = art_diff.run(path, ref_path, "diff-txt", txt_files)
            if txt_files_exit_code != 0:  # pragma: no cover
                if 'diff-txt' not in ignore_exit_codes:
                    result |= txt_files_exit_code
            exit_pattern = self.update_result_pattern(exit_pattern, txt_files_exit_pattern, art_diff.diff_index('diff-txt'), txt_files_exit_code)

        col_names = ["meta-diff", "diff-pool", "diff-root", "tag-diff", "txt-diff"]
        row_names = exit_pattern.keys()
        format_row = "{:>50}" + "{:>13}" * len(col_names)
        print('-' * 120)
        print('ART-Compare: Summary')
        print('-' * 120)
        print(format_row.format("", *col_names))
        for row_name, row in zip(row_names, exit_pattern.values()):
            print(format_row.format(row_name, *row))

        print('-' * 120)
        print('Final Exit: ' + str(result))
        print('-' * 120)

        if not no_out:
            file.close()
            sys.stdout = temp_stdout

        return result

    #
    # Protected Methods
    #
    @staticmethod
    def get_art_results(output):
        """
        Extract art-results.

        find all
        'art-result: x' or 'art-result: x name' or 'art-result: [x]'
        and append them to result list
        """
        result = []
        for line in output.splitlines():
            match = re.search(r"art-result: (\d+)\s*(.*)", line)
            if match:
                item = json.loads(match.group(1))
                name = match.group(2)
                result.append({'name': name, 'result': item})
            else:
                match = re.search(r"art-result: (\[.*\])", line)
                if match:
                    array = json.loads(match.group(1))
                    for item in array:
                        result.append({'name': '', 'result': item})

        return result

    def __run_on(self, nightly_tag, on_days):
        """Return True if this is a good day to run self."""
        log = logging.getLogger(MODULE)
        if on_days is None or len(on_days) == 0:
            return True

        # nightly tag format: 2017-02-26T2119
        # tagday offset by 3 hours, so Saturday 21:00 to Saturday 21:00 is Saturday
        fmt = '%Y-%m-%dT%H%M'
        offset = timedelta(hours=3)
        tagday = datetime.strptime(nightly_tag, fmt) + offset

        for on_day in on_days:
            # list of keys
            days = on_day.split(',')
            for day in days:
                if day == '':
                    continue

                day_name = calendar.day_name[tagday.weekday()].lower()
                week_day = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
                keywords = week_day
                week_end = ['saturday', 'sunday']
                keywords.extend(week_end)
                weekdays = 'weekdays'
                keywords.append(weekdays)
                weekends = 'weekends'
                keywords.append(weekends)
                odd_days = 'odd days'
                keywords.append(odd_days)
                even_days = 'even days'
                keywords.append(even_days)

                day_lowercase = day.lower()
                if day_lowercase in keywords:
                    # Check for Monday, Tuesday, ...
                    if day_lowercase in week_day and day_lowercase == day_name:
                        return True

                    # weekends
                    elif day_lowercase == weekends and day_name in week_end:
                        return True

                    # weekdays
                    elif day_lowercase == weekdays and day_name not in week_end:
                        return True

                    # odd days
                    elif day_lowercase == odd_days and tagday.day % 2 != 0:
                        return True

                    # even days
                    elif day_lowercase == even_days and tagday.day % 2 == 0:
                        return True

                else:
                    # day number
                    try:
                        if int(day) == tagday.day:
                            return True
                    except ValueError:
                        log.warning("Some item in art_runon list '['%s']' is not an integer or valid keyword, skipped", day)

        return False

    def get_files(self, directory, job_type=None, index_type="all", nightly_release=None, project=None, platform=None, run_all_tests=False, no_build=None, valid_only=True, run_on=True, tests=None):
        """
        Return a list of all test files matching 'test_*.sh' of given 'job_type', 'index_type' and nightly/project/platform.

        'job_type' can be 'grid' or 'local', given by the test

        'index_type' can be 'all', 'batch' or 'single'.

        Tests are further filtered by run_all_tests, no_build, valid_only, run_on and list of tests.

        Only the filenames are returned.
        """

        if directory is None or not os.path.isdir(directory):
            return []

        log = logging.getLogger(MODULE)

        tests = [] if tests is None else tests
        result = []

        # figure out nightly tag from environment or from directory structure, if both fail set to date before ATLAS
        nightly_tag = os.getenv('AtlasBuildStamp', get_release(directory)[3])
        nightly_tag = nightly_tag if nightly_tag else '1970-01-01T0000'

        log.debug("AtlasBuildStamp %s", os.getenv('AtlasBuildStamp', None))
        log.debug("Dir 3 %s", get_release(directory)[3])
        log.debug("Nightly Tag %s", nightly_tag)

        files = os.listdir(directory)
        files = [n for n in files if fnmatch.fnmatch(n, 'test_*.sh') or fnmatch.fnmatch(n, 'test_*.py')]
        files.sort()

        nightly_release = self.adjust_branch(directory, files, nightly_release, project, platform)

        for fname in files:
            test_name = os.path.join(directory, fname)
            header = ArtHeader(test_name)
            has_art_input = header.get(ArtHeader.ART_INPUT) is not None
            has_art_athena_mt = header.get(ArtHeader.ART_ATHENA_MT) > 0
            has_art_memory = header.get(ArtHeader.ART_MEMORY) > 0
            has_art_runon = header.get(ArtHeader.ART_RUNON)
            has_art_architecture = header.get(ArtHeader.ART_ARCHITECTURE) is not None
            has_art_no_looping_check = header.get(ArtHeader.ART_NO_LOOPING_CHECK)
            has_art_not_expand_inds = header.get(ArtHeader.ART_NOT_EXPAND_INDS)
            has_art_not_expand_secdss = header.get(ArtHeader.ART_NOT_EXPAND_SECDSS)
            has_art_input_secdss = header.get(ArtHeader.ART_INPUT_SECDSS)
            try:
                has_art_cores_gt_1 = int(header.get(ArtHeader.ART_CORES)) > 1
            except ValueError:
                has_art_cores_gt_1 = False

            execute_nightly_tag = no_build if no_build else nightly_tag

            # SKIP if file is not valid
            if valid_only and not header.is_valid():
                log.debug("%s no valid header, skipped", fname)
                continue

            # SKIP if art-type not defined
            if header.get(ArtHeader.ART_TYPE) is None:
                log.debug("%s art-type not defined, skipped", fname)
                continue

            # SKIP if is not of correct type
            if job_type is not None and header.get(ArtHeader.ART_TYPE) != job_type:
                log.debug("%s not of correct type: %s, while requested %s, skipped", fname, header.get(ArtHeader.ART_TYPE), job_type)
                continue

            # SKIP if is not included in nightly_release, project, platform
            if not run_all_tests and nightly_release is not None and not self.is_included(test_name, nightly_release, project, platform):
                log.debug("%s not included in nightly_release %s, project %s, platform %s, skipped", fname, nightly_release, project, platform)
                continue

            is_single_job = (has_art_input or has_art_athena_mt or has_art_memory)
            is_single_job = is_single_job or (has_art_cores_gt_1 or has_art_architecture)
            is_single_job = is_single_job or (has_art_no_looping_check or has_art_not_expand_inds or has_art_not_expand_secdss or has_art_input_secdss)

            # SKIP if batch and does specify art-input or art-athena-mt
            if index_type == "batch" and is_single_job:
                log.debug("%s batch job with art-input or art-athena-mt or art_memory or art_cores or art-architecture, skipped", fname)
                continue

            # SKIP if single and does NOT specify art-input or art-athena-mt
            if index_type == "single" and not is_single_job:
                log.debug("%s single job without art-input or art-athena-mt or art_memory or art_cores or art-architecture, skipped", fname)
                continue

            # SKIP if no_build and run_on not requested
            if no_build and not run_on:
                log.debug("%s no-build and run_on not requested, skipped", fname)
                continue

            # SKIP if no_build and test does not specify runon
            if no_build and not has_art_runon:
                log.debug("%s no_build and test does not specify runon, skipped", fname)
                continue

            # SKIP if this is not a good day to run
            if run_on and execute_nightly_tag and not self.__run_on(execute_nightly_tag, header.get(ArtHeader.ART_RUNON)):
                log.debug("%s not a good day to run: %s, requested: %s, skipped", fname, execute_nightly_tag, header.get(ArtHeader.ART_RUNON))
                continue

            # SKIP if not in list of tests
            if tests and fname not in tests:
                log.debug("%s if not in list of tests: %s", fname, tests)
                continue

            result.append(fname)

        return result

    def get_type(self, directory, test_name):
        """Return the 'job_type' of a test."""
        return ArtHeader(os.path.join(directory, test_name)).get(ArtHeader.ART_TYPE)

    def get_test_directories(self, directory):
        """
        Search from '<directory>...' for '<package>/test' directories.

        A dictionary key=<package>, value=<directory> is returned
        """
        result = {}
        for root, dirs, _ in scan.walk(directory):
            # exclude some directories
            dirs[:] = [d for d in dirs if not d.endswith('_test.dir')]
            if root.endswith('/test'):
                package = os.path.basename(os.path.dirname(root))
                result[package] = root
        return result

    def get_list(self, directory, package, job_type, index_type):
        """Return a list of tests for a particular package."""
        test_directories = self.get_test_directories(directory)
        test_dir = test_directories[package]
        return self.get_files(test_dir, job_type=job_type, index_type=index_type)

    def adjust_branch(self, directory, files, branch, project, platform):
        """Switches the branch to be the base one, if no tests are found
        in the originally requested branch."""
        if not branch:
            return branch

        for fname in files:
            test_name = os.path.join(directory, fname)
            # FIXME: check that platform and project are not None
            # Found a test in the original branch that will be included,
            # no need to change the branch we run on then.
            if self.is_included(test_name, branch, project, platform):
                return branch

        # Make any main-* release look for tests on main
        for name in ["main"]:
            if branch.startswith(name + "-"):
                branch = name
                break

        return branch

    def is_included(self, test_name, branch, project, platform):
        """Check if a match is found for test_name in the nightly branch, project, platform."""
        assert test_name is not None
        assert branch is not None
        assert project is not None
        assert platform is not None

        def matches(a, b):
            for aa, bb in zip(a, b):
                if not fnmatch.fnmatch(aa, bb):
                    return False
            return True

        nightly_release = [[branch, project, platform]]
        includes = ArtHeader(test_name).get(ArtHeader.ART_INCLUDE) or []
        for art_include in includes:
            for nr in nightly_release:
                art_include_fields = split_release(art_include)
                if matches(nr, art_include_fields):
                    return True

        return False
