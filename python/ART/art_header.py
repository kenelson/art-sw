#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""Class to handle art-headers."""
from __future__ import absolute_import
from __future__ import unicode_literals

from builtins import str
from builtins import object
from builtins import open

from ast import literal_eval as safe_eval
import logging
import re

from .art_misc import is_exe

MODULE = "art.header"


class ArtHeader(object):
    """Class to handle art-headers."""

    # headers in alphabetical order
    ART_ATHENA_MT = 'art-athena-mt'
    ART_CI = 'art-ci'
    ART_CORES = 'art-cores'
    ART_DESCRIPTION = 'art-description'
    ART_ARCHITECTURE = 'art-architecture'
    ART_HTML = 'art-html'
    ART_INCLUDE = 'art-include'
    ART_INPUT = 'art-input'
    ART_INPUT_NEVENTSPERFILE = 'art-input-neventsperfile'
    ART_INPUT_NEVENTSPERJOB = 'art-input-neventsperjob'
    ART_INPUT_NFILES = 'art-input-nfiles'
    ART_INPUT_NFILESPERJOB = 'art-input-nfilesperjob'
    ART_INPUT_NJOBS = 'art-input-njobs'
    ART_INPUT_SECDSS = 'art-input-secdss'
    ART_INPUT_SPLIT = 'art-input-split'
    ART_MEMORY = 'art-memory'
    ART_ENV_KEY_VALUE = 'art-env-key-value'
    ART_NO_LOOPING_CHECK = 'art-no-looping-check'
    ART_NOT_EXPAND_INDS = 'art-not-expand-inds'
    ART_NOT_EXPAND_SECDSS = 'art-not-expand-secdss'
    ART_OUTPUT = 'art-output'
    ART_RUNON = 'art-runon'
    ART_TYPE = 'art-type'

    # Headers to add to/remove from the pathena CLI flags/options
    ART_PATHENA_OPTIONS_ADD = 'art-pathena-options-add'
    ART_PATHENA_OPTIONS_REMOVE = 'art-pathena-options-remove'
    ART_PATHENA_FLAGS_ADD = 'art-pathena-flags-add'
    ART_PATHENA_FLAGS_REMOVE = 'art-pathena-flags-remove'

    def __init__(self, filename):
        """Keep arguments, setup patterns for re-use, define possible art-header definitions."""
        self.header_format = re.compile(r'#\s(art-[\w-]+):\s+(.+)$')
        self.header_format_error1 = re.compile(r'#(art-[\w-]*):\s*(.+)$')
        self.header_format_error2 = re.compile(r'#\s\s+(art-[\w-]*):\s*(.+)$')
        self.header_format_error3 = re.compile(r'#\s(art-[\w-]*):\S(.*)$')
        self.header_format_error4 = re.compile(r'#\s(art-[\w-]+):\s+$')
        self.header_format_error5 = re.compile(r'#\s(art-[\w-]+)\s+')

        self.filename = filename

        self.header = {}

        self.valid = True

        # general
        self.add(ArtHeader.ART_DESCRIPTION, str, '')
        self.add(ArtHeader.ART_INCLUDE, list, [])        # not specifying results in inclusion
        self.add(ArtHeader.ART_RUNON, list, [])
        self.add(ArtHeader.ART_TYPE, str, None, ['build', 'grid', 'local'])

        # "local" type only
        self.add(ArtHeader.ART_CI, list, [])

        # "grid" type only
        self.add(ArtHeader.ART_ATHENA_MT, int, 0)
        self.add(ArtHeader.ART_CORES, int, 1)
        self.add(ArtHeader.ART_ARCHITECTURE, str, None)
        self.add(ArtHeader.ART_ENV_KEY_VALUE, list, [])
        self.add(ArtHeader.ART_HTML, str, None)
        self.add(ArtHeader.ART_INPUT, str, None)
        self.add(ArtHeader.ART_INPUT_NEVENTSPERFILE, int, 0)
        self.add(ArtHeader.ART_INPUT_NEVENTSPERJOB, int, 0)
        self.add(ArtHeader.ART_INPUT_NFILES, int, 0)
        self.add(ArtHeader.ART_INPUT_NFILESPERJOB, int, 0)
        self.add(ArtHeader.ART_INPUT_NJOBS, int, 0)
        self.add(ArtHeader.ART_INPUT_SECDSS, list, [])
        self.add(ArtHeader.ART_INPUT_SPLIT, int, 0)
        self.add(ArtHeader.ART_MEMORY, int, 0)
        self.add(ArtHeader.ART_NO_LOOPING_CHECK, bool, False)
        self.add(ArtHeader.ART_NOT_EXPAND_INDS, bool, False)
        self.add(ArtHeader.ART_NOT_EXPAND_SECDSS, bool, False)
        self.add(ArtHeader.ART_OUTPUT, list, [])

        self.add(ArtHeader.ART_PATHENA_FLAGS_ADD, list, [])
        self.add(ArtHeader.ART_PATHENA_FLAGS_REMOVE, list, [])
        self.add(ArtHeader.ART_PATHENA_OPTIONS_ADD, dict, {})
        self.add(ArtHeader.ART_PATHENA_OPTIONS_REMOVE, list, [])

        self.__read(filename)

    def add(self, key, value_type, default_value=None, constraint=None, defined=True):
        """Add a single header definition."""
        self.header[key] = {}
        self.header[key]['type'] = value_type
        self.header[key]['default'] = default_value
        self.header[key]['constraint'] = constraint
        self.header[key]['defined'] = defined
        self.header[key]['value'] = None    # e.g. the value was never set
        self.header[key]['errors'] = []     # no errors

    def __read(self, filename):
        """Read all headers from file."""
        def header_lines():
            with open(filename, "r", encoding="utf-8") as f:
                for line in f:
                    line = line.strip()
                    match = self.header_format.match(line)
                    if match:
                        key, value = match.group(1), match.group(2).strip()
                        yield (key, value)

        for key, value in header_lines():
            # handle values
            if key not in self.header:
                error = "Unknown art-header " + key + ": " + str(value) + " in file " + filename
                self.add(key, str, defined=False)
                self.header[key]['errors'].append(error)
                self.valid = False
                return

            type_fn = self.header[key]['type']

            # Simple type
            if type_fn in (int, bool, str):
                try:
                    value = type_fn(value)
                except ValueError:
                    error = "Invalid value in art-header " + key + ": " + str(value) + " in file " + filename
                    self.header[key]['errors'].append(error)
                    self.valid = False
                    return
                else:
                    if self.header[key]['value'] is not None:
                        error = "Key " + key + ": already set to '" + str(self.header[key]['value']) + "' in file " + filename
                        self.header[key]['errors'].append(error)
                        self.valid = False
                        return
                    self.header[key]['value'] = value

            # Complex types
            elif type_fn is list:
                # Initialise to the empty type
                if self.header[key]['value'] is None:
                    self.header[key]['value'] = []

                # A list item was given, so we extend rather than append
                try:
                    value = safe_eval(value)
                except ValueError:
                    # This will trigger if we try and eval a string that is not a literal
                    pass
                except SyntaxError:
                    # Try surrounding with quotes
                    value = "'" + value + "'"
                    try:
                        value = safe_eval(value)
                    except SyntaxError:
                        self.valid = False
                        self.header[key]['errors'].append("Syntax error for key:value " + str(key) + str(value))
                        return
                    except ValueError:
                        pass
                else:
                    if isinstance(value, set) or isinstance(value, dict):
                        self.valid = False
                        self.header[key]['errors'].append("Illegal value for key " + str(key) + ": " + str(value))
                        return

                if isinstance(value, list):
                    self.header[key]['value'].extend(value)
                else:
                    self.header[key]['value'].append(value)

            elif type_fn is dict:
                if self.header[key]['value'] is None:
                    self.header[key]['value'] = {}

                try:
                    value = safe_eval(value)
                except (ValueError, SyntaxError):
                    error = "Invalid value in art-header " + key + ": " + str(value) + " in file " + filename
                    self.header[key]['errors'].append(error)
                    self.valid = False
                    return
                else:
                    if not isinstance(value, dict):
                        error = "Invalid value in art-header " + key + ": " + str(value) + " in file " + filename
                        self.header[key]['errors'].append(error)
                        self.valid = False
                        return

                    self.header[key]['value'].update(value)

            # further Validation: type incorrect and not None
            if not isinstance(self.header[key]['value'], self.header[key]['type']) and not isinstance(self.header[key]['value'], type(None)):
                error = "Type incorrect: " + str(type(self.header[key]['value'])) + " not valid for key: " + key + ", expected " + str(self.header[key]['type']) + " in file " + filename
                self.header[key]['errors'].append(error)
                self.valid = False
                return

            # further Validation: constraint not correct
            if self.header[key]['constraint'] is not None and self.header[key]['value'] not in self.header[key]['constraint']:
                error = "Value: " + str(self.header[key]['value']) + " for key: " + key + " not in constraints: " + str(self.header[key]['constraint']) + " in file " + filename
                self.header[key]['errors'].append(error)
                self.valid = False
                return

    def is_valid(self):
        """Return True if file was read without errors."""
        return self.valid

    def get(self, key):
        """
        Get the value of a header by key.

        Return default if header not specified.
        Warn and return None if header is not defined.
        """
        log = logging.getLogger(MODULE)
        if key not in self.header:
            log.warning("Art seems to look for a header key %s which is not in the list of defined headers: %s", key, list(self.header.keys()))
            return None

        value = self.header[key]['default'] if self.header[key]['value'] is None else self.header[key]['value']

        # build and local compatibility
        if key == ArtHeader.ART_TYPE and value == 'build':
            value = 'local'

        return value

    def validate(self):
        """
        Validate the '# art-*' headers in the file.

        Validation fails if:
        - test is not executable
        - a header is spaced correctly (e.g. '#art-header: value')
        - a value in a header is not spaced correctly (e.g. '# art-header:value')
        - errors were found during reading (validation in read)

        return True is valid
        """
        log = logging.getLogger(MODULE)
        log.info(self.filename)
        valid = True

        # important
        if not is_exe(self.filename):
            valid = False
            log.error("%s is NOT executable.", self.filename)

        # cosmetics
        with open(self.filename, "r", encoding="utf-8") as f:
            for line in f:
                if self.header_format_error1.match(line):
                    valid = False
                    log.error("LINE: %s", line.rstrip())
                    log.error("Header Validation - invalid header format, use space between '# and art-xxx' in file %s", self.filename)
                if self.header_format_error2.match(line):
                    valid = False
                    log.error("LINE: %s", line.rstrip())
                    log.error("Header Validation - invalid header format, too many spaces between '# and art-xxx' in file %s", self.filename)
                if self.header_format_error3.match(line):
                    valid = False
                    log.error("LINE: %s", line.rstrip())
                    log.error("Header Validation - invalid header format, use at least one space between ': and value' in file %s", self.filename)
                if self.header_format_error4.match(line):
                    valid = False
                    log.error("LINE: %s", line.rstrip())
                    log.error("Header Validation - invalid header format, missing argument after ': ' in file %s", self.filename)
                if self.header_format_error5.match(line):
                    valid = False
                    log.error("LINE: %s", line.rstrip())
                    log.error("Header Validation - invalid header format, missing colon after 'art-* ' in file %s", self.filename)

        if not self.get(ArtHeader.ART_TYPE):
            valid = False
            log.error("Missing art-type header in file %s", self.filename)

        if len(self.get(ArtHeader.ART_INCLUDE)) <= 0:
            valid = False
            log.error("No art-include header file %s", self.filename)

        # report errors found during __read
        for key in self.header:
            for error in self.header[key]['errors']:
                valid = False
                log.error(error)

        return valid
