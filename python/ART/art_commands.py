#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""Class commands."""
from __future__ import print_function
from __future__ import unicode_literals

from builtins import str

import os
import sys

from .art_base import ArtBase
from .art_grid import ArtGrid
from .art_local import ArtLocal
from .art_misc import get_atlas_env, local_script_directory, set_log  # noqa: E402

ARTPROD = 'artprod'


def fail(args, _art_directory):
    print("Fail", args)
    sys.exit()


def compare(args, art_directory):
    if args['ref']:
        compare_ref(args, art_directory)
    elif args['grid']:
        compare_grid(args, art_directory)
    else:
        fail(args, art_directory)


def compare_ref(args, _art_directory):
    """Compare the output of a job."""
    set_log(args)
    path = args['<path>']
    ref_path = args['<ref_path>']
    files = args['--file']
    txt_files = args['--txt-file']
    no_diff_meta = args['--no-diff-meta']
    diff_meta = args['--diff-meta']
    diff_pool = args['--diff-pool']
    diff_root = args['--diff-root']
    if not txt_files and not diff_meta and not diff_pool and not diff_root:
        diff_pool = True
        diff_root = True
    diff_meta = diff_meta or diff_pool or diff_root
    diff_meta = diff_meta and not no_diff_meta
    entries = args['--entries']
    mode = args['--mode']
    order_trees = args['--order-trees']
    exclude_vars = args['--exclude-var']
    ignore_leaves = args['--ignore-leave']
    branches_of_interest = args['--branch-of-interest']
    exact_branches = args['--exact-branches']
    meta_mode = args['--meta-mode']
    no_out = args['--no-out']
    ignore_exit_codes = args['--ignore-exit-code']
    sys.exit(ArtBase().compare_ref(path, ref_path, files, txt_files, diff_meta, diff_pool, diff_root, ignore_exit_codes=ignore_exit_codes, entries=entries, mode=mode, order_trees=order_trees, exclude_vars=exclude_vars, ignore_leaves=ignore_leaves, branches_of_interest=branches_of_interest, exact_branches=exact_branches, meta_mode=meta_mode, no_out=no_out))


def compare_grid(args, art_directory):
    """Compare the output of a job."""
    set_log(args)
    package = args['<package>']
    test_name = args['<test_name>']
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    max_refs = int(args['--max-refs'])
    group_user = _get_group_user(args)
    files = args['--file']
    txt_files = args['--txt-file']
    no_diff_meta = args['--no-diff-meta']
    diff_meta = args['--diff-meta']
    diff_pool = args['--diff-pool']
    diff_root = args['--diff-root']
    if not txt_files and not diff_meta and not diff_pool and not diff_root:
        diff_pool = True
        diff_root = True
    diff_meta = diff_meta or diff_pool or diff_root
    diff_meta = diff_meta and not no_diff_meta
    entries = args['--entries']
    mode = args['--mode']
    order_trees = args['--order-trees']
    exclude_vars = args['--exclude-var']
    ignore_leaves = args['--ignore-leave']
    branches_of_interest = args['--branch-of-interest']
    exact_branches = args['--exact-branches']
    meta_mode = args['--meta-mode']
    no_out = args['--no-out']
    ignore_exit_codes = args['--ignore-exit-code']
    sys.exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag, None, 0, False, None).compare(package, test_name, max_refs, group_user, files, txt_files, diff_meta, diff_pool, diff_root, ignore_exit_codes=ignore_exit_codes, entries=entries, mode=mode, order_trees=order_trees, exclude_vars=exclude_vars, ignore_leaves=ignore_leaves, branches_of_interest=branches_of_interest, exact_branches=exact_branches, meta_mode=meta_mode, shell=True, no_out=no_out))


def list(args, art_directory):
    """List the jobs of a package."""
    if not args['grid']:
        fail(args, art_directory)
    set_log(args)
    package = args['<package>']
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    print(nightly_release, project, platform, nightly_tag)
    job_type = 'grid'
    index_type = args['--test-type']
    json_format = args['--json']
    group_user = _get_group_user(args)
    sys.exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag, None, 0, False, None).list(package, job_type, index_type, json_format, group_user))


def output(args, art_directory):
    """Get the output of a job."""
    if not args['grid']:
        fail(args, art_directory)
    set_log(args)
    package = args['<package>']
    test_name = args['<test_name>']
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    group_user = _get_group_user(args)
    sys.exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag, None, 0, False, None).output(package, test_name, group_user))


def submit(args, art_directory):
    """Submit nightly jobs to the grid, NOT for users."""
    set_log(args)
    sequence_tag = args['<sequence_tag>']
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    job_type = _get_job_type(args, 'grid')
    group_user = _get_group_user(None)
    inform_panda = group_user == 'group.art'
    packages = args['<packages>']
    config = args['--config']
    no_action = args['--no-action']
    max_jobs = int(args['--max-jobs'])
    wait_and_copy = not args['--no-copy']
    run_all_tests = args['--run-all-tests']
    no_build = args['--no-build']

    # only for unittest, make sure to use .get()
    script_directory = args.get('<script_directory>', None)
    inform_panda = args.get('--inform-panda', inform_panda)
    wait = args.get('--wait', ArtGrid.INITIAL_RESULT_WAIT_INTERVAL)

    sys.exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag, script_directory, max_jobs, run_all_tests, no_build).task_list(job_type, sequence_tag, group_user, inform_panda, packages, no_action, wait_and_copy, config, wait=wait))


def grid(args, art_directory):
    """Run jobs from a package on the grid, needs release and grid setup."""
    set_log(args)
    script_directory = args['<script_directory>']
    sequence_tag = args['<sequence_tag>']
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    job_type = _get_job_type(args, 'grid')
    inform_panda = False
    packages = []
    tests = []  # args['--test_names']
    config = None
    no_action = args['--no-action']
    max_jobs = int(args['--max-jobs'])
    wait_and_copy = False
    run_all_tests = args['--run-all-tests']
    no_build = args['--no-build']
    group_user = _get_group_user(None)

    # only for unittest, make sure to use .get()
    wait = args.get('wait', ArtGrid.INITIAL_RESULT_WAIT_INTERVAL)

    sys.exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag, script_directory, max_jobs, run_all_tests, no_build=no_build).task_list(job_type, sequence_tag, group_user, inform_panda, packages, no_action, wait_and_copy, config, wait=wait, tests=tests))


def run(args, art_directory):
    """Run jobs from a package on local machines, needs release and grid setup."""
    set_log(args)
    script_directory = args['<script_directory>']
    sequence_tag = args['<sequence_tag>']
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    job_type = _get_job_type(args, 'local')
    tests = args['<test_names>']
    run_all_tests = args['--run-all-tests']
    timeout = int(args['--timeout'])
    copy_option = args['--copy']
    sys.exit(ArtLocal(art_directory, nightly_release, project, platform, nightly_tag, script_directory=script_directory, max_jobs=int(args['--max-jobs']), ci=args['--ci'], run_all_tests=run_all_tests).task_list(job_type, sequence_tag, timeout=timeout, copy=copy_option, tests=tests))


def copy(args, art_directory):
    """Copy outputs to eos area."""
    set_log(args)
    indexed_package = args['<indexed_package>']
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    # NOTE: default depends on USER, not set it here but in ArtGrid.copy
    dst = args['--dst']
    group_user = _get_group_user(args)
    default_dst = ArtGrid.EOS_OUTPUT_DIR if group_user == 'group.art' else '.'
    dst = default_dst if dst is None else dst
    unpack = args['--unpack']
    tmp = args['--tmp']
    seq = int(args['--seq'])
    keep_tmp = args['--keep-tmp']
    sys.exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag, None, 0, False, None).copy(indexed_package, dst, group_user, unpack=unpack, tmp=tmp, seq=seq, keep_tmp=keep_tmp))


def validate(args, _art_directory):
    """Check headers in tests."""
    set_log(args)
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    script_directory = local_script_directory(args['<script_directory>'], nightly_release, project, platform, nightly_tag)
    sys.exit(ArtBase().validate(script_directory))


def included(args, _art_directory):
    """Show list of files which will be included for art submit/art grid."""
    set_log(args)
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    job_type = _get_job_type(args, None)  # None will list all types
    index_type = args['--test-type']
    script_directory = local_script_directory(args['<script_directory>'], nightly_release, project, platform, nightly_tag)
    packages = args['<packages>']
    result = ArtBase().included(script_directory, job_type, index_type, packages, nightly_release, project, platform)
    for test_name, include in sorted(result.items()):
        print(str(test_name), str(include))
    sys.exit(0)


def input_collect(args, _art_directory):
    """Scan latest test directories for declarations of art-input."""
    set_log(args)
    job_type = _get_job_type(args, 'grid')
    run_all_tests = args['--run-all-tests']
    copy_option = args['--copy']
    ART_INPUT_REFERENCE = '/eos/atlas/atlascerngroupdisk/data-art/grid-output/art-input-reference'
    copy_option = ART_INPUT_REFERENCE if copy_option is None else copy_option
    release = args['<release>']
    sys.exit(ArtBase().input_collect(job_type, run_all_tests, copy_option, release))


def input_merge(args, _art_directory):
    """Merge all input files and compares to reference file."""
    set_log(args)
    work_dir = args['--work-dir']
    ref_file = args['--ref-file']
    sys.exit(ArtBase().input_merge(work_dir=work_dir, ref_file=ref_file))


def configuration(args, _art_directory):
    """Show configuration."""
    set_log(args)
    package = args['<package>']
    (nightly_release, project, platform, _nightly_tag) = get_atlas_env()
    config = args['--config']
    sys.exit(ArtBase().config(package, nightly_release, project, platform, config))


def createpoolfile(args, art_directory):  # pragma: no cover
    """Create PoolFileCatalog."""
    set_log(args)
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    sys.exit(ArtGrid(art_directory, nightly_release, project, platform, nightly_tag, None, 0, False, None).createpoolfile())


def download(args, art_directory):
    """Download the output of a reference job."""
    set_log(args)
    package = args['<package>']
    test_name = args['<test_name>']
    (nightly_release, project, platform, nightly_tag) = get_atlas_env()
    max_refs = int(args['--max-refs'])
    group_user = _get_group_user(args)
    dst = args['--dst']
    ref_nightly_release = args['--nightly-release']
    ref_project = args['--project']
    ref_platform = args['--platform']

    # only for unittest, make sure to use .get()
    max_tries = args.get('--max-tries', 3)

    ref = ArtGrid(art_directory, nightly_release, project, platform, nightly_tag, None, 0, False, None).download(package, test_name, max_refs, group_user, ref_dir=dst, shell=True, max_tries=max_tries, nightly_release=ref_nightly_release, project=ref_project, platform=ref_platform)
    sys.exit(1 if ref is None else 0)


def _get_job_type(args, default):
    """Retrieve type."""
    job_type = default if args['--type'] is None else args['--type']
    if job_type == 'build':
        job_type = 'local'
    return job_type


def _get_group_user(args):
    """
    Retrieve 'group.user'.

    By default ARTPROD user is used.
    For most commands the user can be overridden by --user=<USER>.
    For "art grid" the logged in USER is used, as this is a user command.
    For "art submit" the logged in USER (ARTPROD) is used.

    When run in the grid, the user may be anything, which is why the default user is ARTPROD
    """
    user = None

    # args is None, no option override, e.g. art grid and art submit
    user = os.getenv('USER', ARTPROD) if args is None else user

    # --user, override option given
    user = args.get('--user', None) if user is None else user

    # default, ARTPROD
    user = ARTPROD if user is None else user
    group = 'user'

    # handle conversion to group.art if user is artprod
    if user == ARTPROD:
        user = 'art'
        group = 'group'

    return group + '.' + user
