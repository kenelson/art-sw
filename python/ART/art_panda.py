from collections import namedtuple
import http.client
import logging
import json

MODULE = "art.panda"


def http_post(url, payload, headers=None, timeout=-1):
    # requests not available on lxplus, import only when needed
    import requests

    try:
        resp = requests.post(url, data=json.dumps(payload), headers=headers, timeout=timeout, verify=False)
        return (resp, None)
    except requests.RequestException as e:  # pragma: no cover
        # Base exception for requests library, should capture all
        return (None, e)
    except Exception as e:
        return (None, e)


def http_get(url, payload):
    import requests
    try:
        r = requests.get(url, params=payload, verify=False)
        if r.ok:  # status_code == 200
            s = r.json()
            if 'message' in s:
                msg = "Error reply from server: {}\n".format(s['message'])
                msg += "Lost connection with {}\n".format(url)
                return [], Exception(msg)
            return (r.json(), None)
        return [], Exception("Bad HTTP status code")
    except requests.exceptions.RequestException as e:
        return [], e
    except http.client.IncompleteRead as e:
        return [], e


def panda_task_status(payload):
    log = logging.getLogger(MODULE)
    url = 'https://bigpanda.cern.ch/tasks/'
    tasks, exc = http_get(url, payload)
    if exc:
        log.error("Fetching tasks from panda failed., Error: {}".format(exc))
        return []
    return tasks


def get_panda_informer(is_no_op=True):
    def no_op(*args, **kws):
        pass

    __panda_inform = namedtuple('panda_inform', 'job_started job_done')
    if is_no_op:
        return __panda_inform(no_op, no_op)
    return __panda_inform(job_started, job_done)


def _headers():
    agent = 'Mozilla/5.0 (X11; Linux x86_64) '
    agent += 'AppleWebKit/537.36 (KHTML, like Gecko) '
    agent += 'Chrome/51.0.2704.103 Safari/537.36'
    return {'User-agent': agent}


def log_err(logger, prefix_msg):
    """Helper function to log error messages"""
    def attempt(number):
        def emit(msg, *args):
            msg = "Attempt {} {}. {}".format(number, prefix_msg, msg.format(*args))
            logger.error(msg)
        return emit
    return attempt


def job_started(payload):
    """Inform panda that this job has started."""
    log = logging.getLogger(MODULE)

    panda_id = payload['pandaid']
    job_name = "{}/{}".format(payload['package'], payload['testname'])
    log.info("Registering job {} ({}) with bigpanda".format(job_name, panda_id))

    url = "http://bigpanda.cern.ch/art/registerarttest/?json"
    max_attempts = 3

    err_log_factory = log_err(log, "to register job failed")
    for attempt in range(1, max_attempts + 1):
        error = err_log_factory(attempt)
        resp, exc = http_post(url, payload, timeout=120, headers=_headers())
        if exc is not None:
            error("Error: {}", exc)
        elif resp is None:
            error("HTTP Response was empty.")
        elif not resp.ok:
            error("Got HTTP status code {}", resp.status_code)
        else:
            try:
                resp = resp.json()
            except ValueError as e:  # pragma: no cover
                error("Corrupted HTTP Post response: {}", e)
            else:
                if 'exit_code' not in resp:
                    error("Panda response had no 'exit code' key")
                elif resp['exit_code'] != 0:
                    error("Panda response exit code was non-zero: {}", resp['exit_code'])
                else:
                    # all ok
                    log.info("Registered job {} OK".format(payload['pandaid']))
                    return
    log.error("Failed to inform panda about job {} starting".format(payload['pandaid']))


def job_done(payload):
    """Inform panda that this job is done."""
    log = logging.getLogger(MODULE)
    log.info("Sending job results to panda")

    url = "http://bigpanda.cern.ch/art/uploadtestresult/?json"
    max_attempts = 3

    log.info("Payload type is {}, value is {}".format(type(payload), payload))
    err_log_factory = log_err(log, "to send job results failed")
    for attempt in range(1, max_attempts + 1):
        error = err_log_factory(attempt)
        resp, exc = http_post(url, payload, timeout=120, headers=_headers())
        if exc is not None:
            error("Error: {}", exc)
        elif resp is None:
            error("HTTP Response was empty.")
        elif not resp.ok:
            error("Got HTTP status code {}", resp.status_code)
            try:
                resp = resp.json()
            except ValueError as e:
                error("Corrupted HTTP response JSON: {}", e)
            else:
                if "message" in resp:
                    log.error(resp["message"])
        else:
            try:
                resp = resp.json()
            except ValueError as e:
                error("Corrupted HTTP response JSON: {}", e)
            else:
                if "message" in resp:
                    log.info(resp["message"])
                log.info("Sent job results to Panda OK")
                return
    log.error("Failed to inform panda about job done")
