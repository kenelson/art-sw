#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""Interface to the general ART configuration."""
from __future__ import unicode_literals

from builtins import object
from builtins import open

from functools import cmp_to_key

import fnmatch
import logging
import os
import yaml

MODULE = "art.configuration"


class ArtConfiguration(object):
    """Class to interface to the ART configuration."""

    ALL = 'All'
    SEPARATOR = '/'

    def __init__(self, config_file=None):
        """Init."""
        log = logging.getLogger(MODULE)
        if config_file is None:
            config_file = 'art-configuration.yml'
        full_path = os.path.join(os.getcwd(), config_file)
        try:
            f = open(config_file, "r", encoding='UTF-8')
            # check for existence of method, pyyaml cc7 has it, pyyaml slc6 does not
            if hasattr(yaml, 'full_load') and callable(getattr(yaml, 'full_load')):
                self.config = yaml.full_load(f)
            else:
                self.config = yaml.load(f)
            log.info("Loaded ART Configuration from %s", full_path)
            f.close()
        except IOError:
            log.critical("Cannot read ART Configuration from %s", full_path)
            self.config = None

    def release_key(self, branch, project, platform):
        """
        Return release key.

        Format is: /21.0/Athena/x86_64-slc6-gcc62-opt
        """
        return ArtConfiguration.SEPARATOR + ArtConfiguration.SEPARATOR.join((branch, project, platform))

    def release_key_compare(self, x, y):
        """Compare two release keys."""
        xa = x.split(ArtConfiguration.SEPARATOR)
        ya = y.split(ArtConfiguration.SEPARATOR)

        for index, _item in sorted(enumerate(xa), reverse=True):
            if xa[index] < ya[index]:
                return -1
            elif xa[index] > ya[index]:
                return +1
        return 0

    def keys(self, branch, project, platform, package=None):
        """Return all keys for all matching patterns for one specific package."""
        if self.config is None:
            return set([])

        if package is None:
            package = ArtConfiguration.ALL

        if package not in self.config:
            return set([])

        keys = set()
        for pattern in self.config[package]:
            if fnmatch.fnmatch(self.release_key(branch, project, platform), pattern):
                for key in list(self.config[package][pattern].keys()):
                    keys.add(key)

        return keys

    def packages(self):
        """Return all packages, including 'All', defined in the configuration."""
        init_list = []
        if self.config is not None:
            init_list = self.config.keys()

        return set(init_list)

    def get(self, branch, project, platform, package, key, default_value=None):
        """Return most specific value for specified key and matching pattern.

        By specifying more specific release_keys in the file [/21.0/*...]
        one can override less specific keys [/*/*...]
        (order in the file is not important):

        Tier0ChainTests:
            /*/*/*:
                dst: /yourlocaldirectory
            /21.0/*/*:
                dst: root://eosatlas.cern.ch//eos/atlas/atlascerngroupdisk/data-art/grid-output

        """
        log = logging.getLogger(MODULE)
        log.debug("Looking for %s %s %s %s %s", branch, project, platform, package, key)
        if self.config is None:
            log.debug("No configuration")
            return default_value

        if package is None:
            log.debug("%s used for package", ArtConfiguration.ALL)
            return self.get(branch, project, platform, ArtConfiguration.ALL, key, default_value)

        if package not in self.config:
            log.debug("%s not in config", package)
            return default_value

        value = None
        for pattern in sorted(self.config[package], key=cmp_to_key(self.release_key_compare)):
            release_key = self.release_key(branch, project, platform)
            log.debug("release_key %s", release_key)
            # print key, pattern
            if fnmatch.fnmatch(release_key, pattern):
                log.debug("matched %s", pattern)
                release = self.config[package][pattern]
                if key in release:
                    value = release[key]

        if value is None:
            if package == ArtConfiguration.ALL:
                value = default_value
            else:
                value = self.get(branch, project, platform, ArtConfiguration.ALL, key, default_value)

        log.debug("Value %s", value)
        return value

    def get_option(self, branch, project, platform, package, key, option_key):
        """TBD."""
        value = self.get(branch, project, platform, package, key)
        return option_key + value if value is not None else ''
