#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""Class for ART Unitttests."""
from __future__ import unicode_literals

from builtins import open

import os
import unittest
import logging

from ART.art_misc import set_log_level

MODULE = "art.testcase"


class ArtTestCase(unittest.TestCase):
    """Class for ART Unitttests."""

    def __init__(self, *args, **kwargs):
        """Init."""
        super(ArtTestCase, self).__init__(*args, **kwargs)
        set_log_level(logging.WARN)

        log = logging.getLogger(MODULE)
        # self.art_python_directory = os.path.dirname(os.path.realpath(__file__))
        modpath = __file__

        # Turn pyc files into py files if we can
        if modpath.endswith('.pyc') and os.path.exists(modpath[:-1]):
            modpath = modpath[:-1]

        self.art_python_directory = os.path.dirname(os.path.realpath(modpath))
        self.art_install_directory = os.path.realpath(os.path.join(self.art_python_directory, '..', '..'))  # remove python/ART
        self.art_directory = os.path.join(self.art_install_directory, 'scripts')
        self.test_directory = os.path.join(self.art_install_directory, 'test')

        log.info("Python directory :  %s", self.art_python_directory)
        log.info("Install directory :  %s", self.art_install_directory)
        log.info("ART directory : %s", self.art_directory)
        log.info("Tests directory : %s", self.test_directory)

    def read_text(self, filename):
        """Open the file in text mode, read it, and close the file."""
        with open(filename, mode='r', encoding='UTF-8') as f:
            return f.read()
