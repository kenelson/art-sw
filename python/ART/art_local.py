#!/usr/bin/env python
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
"""Class for local submits."""
from __future__ import absolute_import
from __future__ import division
from __future__ import unicode_literals

from builtins import str
from builtins import open
from past.utils import old_div

import collections
import concurrent.futures
import fnmatch
import json
import logging
import multiprocessing
import os
import socket
import yaml

from .art_misc import interrupt_command, is_exe, memory, mkdir, run_command, tar, xrdcp, GByte
from .art_base import ArtBase
from .art_header import ArtHeader

from datetime import datetime

MODULE = "art.local"


class ArtLocal(ArtBase):
    """Class for local submits."""

    def __init__(self, art_directory, nightly_release, project, platform, nightly_tag, script_directory, max_jobs=0, ci=False, run_all_tests=False):
        """Keep arguments."""
        super(ArtLocal, self).__init__()
        log = logging.getLogger(MODULE)
        self.art_directory = art_directory
        self.script_directory = os.path.abspath(script_directory).rstrip("/")
        log.debug("ArtLocal %s %s %d", art_directory, script_directory, max_jobs)

        self.nightly_release = nightly_release
        self.project = project
        self.platform = platform
        self.nightly_tag = nightly_tag
        job_mem = 8
        min_cores = 4
        max_tasks = 15      # can be large as the task themselves are just waiting on jobs
        mem = memory(GByte)
        max_cores = min(old_div(mem, job_mem), multiprocessing.cpu_count())
        max_cores = max_cores if max_cores >= min_cores else 1
        self.max_jobs = max_cores if max_jobs <= 0 else max_jobs
        self.ci = ci
        self.run_all_tests = run_all_tests

        log.info("Task and Job Executor started with %d threads", self.max_jobs)
        self.task_executor = concurrent.futures.ThreadPoolExecutor(max_tasks)
        self.job_executor = concurrent.futures.ThreadPoolExecutor(max_workers=self.max_jobs)

    def task_list(self, job_type, sequence_tag, timeout=0, copy=None, tests=None):
        """Run a list of packages for given job_type with sequence_tag."""
        log = logging.getLogger(MODULE)
        if tests is None:
            tests = []
        log.debug("task_list %s %s", job_type, sequence_tag)
        log.info("Timeout %d seconds", timeout)
        test_directories = self.get_test_directories(self.script_directory)
        if not test_directories:
            log.warning('No tests found in directories ending in "test"')
            return 0

        future_task_set = {}

        for package, directory in list(test_directories.items()):
            test_names = self.get_files(directory, job_type=job_type, index_type="all", nightly_release=self.nightly_release, project=self.project, platform=self.platform, run_all_tests=self.run_all_tests, tests=tests)
            if not test_names:
                log.debug("No tests found for package %s and job_type %s", package, job_type)
            else:
                future_task = self.task_executor.submit(self.task, package, job_type, sequence_tag, directory, test_names, timeout, copy)
                future_task_set[future_task] = package

        result = 0
        no_of_tests = 0
        # Wait for all tasks to finish
        for future_task in concurrent.futures.as_completed(future_task_set.keys()):
            log.debug("Package Result: %d (%d tests) for %s", future_task.result()[0], future_task.result()[1], future_task_set[future_task])
            result |= future_task.result()[0]
            no_of_tests += future_task.result()[1]

        # Create general status
        status = collections.defaultdict(lambda: collections.defaultdict(lambda: collections.defaultdict()))

        # Some release information
        status['release_info']['nightly_release'] = self.nightly_release
        status['release_info']['nightly_tag'] = self.nightly_tag
        status['release_info']['project'] = self.project
        status['release_info']['platform'] = self.platform
        status['release_info']['hostname'] = socket.gethostname()
        status['release_info']['no_of_tests'] = no_of_tests

        # write general json file
        mkdir(sequence_tag)
        general_json = os.path.join(sequence_tag, "status.json")
        with open(general_json, 'w', encoding='UTF-8') as outfile:
            outfile.write(str(json.dumps(status, sort_keys=True, indent=4, ensure_ascii=False)))

        if copy:
            log.info("Creating %s and copying general results to there", copy)
            # mkdir(copy)
            copy_exit_code = xrdcp(general_json, copy)
            log.info("Copy Exit Code: %d", copy_exit_code)

        return result

    def task(self, package, job_type, sequence_tag, test_directory, test_names, timeout, copy):
        """Run tests of a single package."""
        log = logging.getLogger(MODULE)
        log.debug("task %s %s %s", package, job_type, sequence_tag)

        if copy:
            mkdir(copy)

        return_code = 0
        future_job_set = {}
        job_index = 0
        no_of_tests = 0
        for test_name in test_names:
            schedule_test = False
            fname = os.path.join(test_directory, test_name)
            log.info("Test name: %s", fname)
            if self.ci:
                branch_name = self.nightly_release  # os.environ['AtlasBuildBranch']
                cis = ArtHeader(fname).get(ArtHeader.ART_CI)
                for ci in cis:
                    if fnmatch.fnmatch(branch_name, ci):
                        schedule_test = True
                        break
            else:
                schedule_test = True

            if not os.access(fname, os.X_OK):  # pragma: no cover
                schedule_test = False
                log.warning("job skipped, file not executable: %s", fname)
                return_code = 1

            if schedule_test:
                # proc keeps track of pid of running job
                proc = {}
                future_job = self.job_executor.submit(self.job, sequence_tag, package, job_type, test_directory, test_name, job_index, proc, copy)
                future_job_set[future_job] = {"name": test_name, "proc": proc}
                no_of_tests += 1
            job_index += 1

        # Create status of all tests
        status = collections.defaultdict(lambda: collections.defaultdict(lambda: collections.defaultdict()))

        # timeout is handled here by killing the job if needed
        timeout = None if timeout == 0 else timeout
        try:
            for future_job in concurrent.futures.as_completed(list(future_job_set.keys()), timeout=timeout):
                # job was 'done'
                (test_name, exit_code, start_time, end_time, test_directory, description, result) = future_job.result()
                return_code |= self.handle_job_result(status, package, test_name, exit_code, start_time, end_time, test_directory, description, result)
        except concurrent.futures.TimeoutError:
            # timeout, not interrupt jobs
            interrupted_set = concurrent.futures.wait(list(future_job_set.keys()), timeout=0).not_done
            for future_task in interrupted_set:
                # cancel all the not_done jobs
                future_task.cancel()

                # kill all running jobs
                if 'value' in future_job_set[future_task]['proc']:
                    interrupt_command(future_job_set[future_task]['proc']['value'], name=future_job_set[future_task]['name'])

            for future_task in interrupted_set:
                try:
                    # job was 'not_done'
                    (test_name, exit_code, start_time, end_time, test_directory, description, result) = future_task.result()
                    return_code |= self.handle_job_result(status, package, test_name, exit_code, start_time, end_time, test_directory, description, result)
                except concurrent.futures.CancelledError:
                    # job was 'cancelled'
                    return_code |= self.handle_job_result(status, package, future_job_set[future_task]['name'], -9, datetime.now(), datetime.now(), test_directory, "Not Started", None)

        # write package json file
        package_json = os.path.join(sequence_tag, package + "-status.json")
        with open(package_json, 'w') as outfile:
            outfile.write(str(json.dumps(status, sort_keys=True, indent=4, ensure_ascii=False)))

        if copy:
            log.info("Copying package json to %s", copy)
            copy_exit_code = xrdcp(package_json, copy)
            log.info("Copy Exit Code: %d", copy_exit_code)

        return (return_code, no_of_tests)

    def handle_job_result(self, status, package, test_name, exit_code, start_time, end_time, test_directory, description, result):
        """Handle the result of done, interrupted or cancelled jobs."""
        log = logging.getLogger(MODULE)
        log.debug("Job Result: %d for %s", exit_code, test_name)
        log.debug("Handling job for %s %s", package, test_name)
        status[package][test_name]['exit_code'] = exit_code
        status[package][test_name]['start_time'] = start_time.strftime('%Y-%m-%dT%H:%M:%S')
        status[package][test_name]['end_time'] = end_time.strftime('%Y-%m-%dT%H:%M:%S')
        status[package][test_name]['start_epoch'] = start_time.strftime('%s')
        status[package][test_name]['end_epoch'] = end_time.strftime('%s')
        status[package][test_name]['result'] = result
        status[package][test_name]['description'] = description
        status[package][test_name]['test_directory'] = test_directory

        # pick up potential art.yml
        yml_file = os.path.join(test_directory, 'art.yml')
        if os.path.isfile(yml_file):
            with open(yml_file, 'r') as stream:
                try:
                    status[package][test_name].update(yaml.safe_load(stream))
                except yaml.YAMLError as exc:  # pragma: no cover
                    log.warning(exc)

        return exit_code

    def job(self, sequence_tag, package, job_type, test_directory, test_name, job_index, proc, copy):
        """Run a single test."""
        log = logging.getLogger(MODULE)
        log.info("Job started %s %s %s %s %s %d", sequence_tag, package, job_type, test_directory, test_name, job_index)
        test_file = os.path.join(test_directory, test_name)

        description = ArtHeader(test_file).get(ArtHeader.ART_DESCRIPTION)

        # check test to be executable
        if not is_exe(test_file):
            log.error("%s is NOT executable.", test_file)
            return (test_name, 2, '', '', test_directory, description, [])

        base_test_name = os.path.splitext(test_name)[0]
        work_directory = os.path.join(sequence_tag, package, base_test_name)
        mkdir(work_directory)
        log.debug("Work dir %s", work_directory)

        # Tests are called with arguments: PACKAGE TEST_NAME SCRIPT_DIRECTORY TYPE
        script_directory = '.'
        env = os.environ.copy()
        env['ArtScriptDirectory'] = script_directory
        env['ArtPackage'] = package
        env['ArtJobType'] = job_type
        env['ArtJobName'] = test_name
        cmd = ' '.join((test_file, package, test_name, script_directory, job_type))
        try:
            log.info("Start:  %s", datetime.now().strftime('%Y-%m-%dT%H:%M:%S'))
            log.info("Executing: %s %s %d", cmd, work_directory, job_index)
            (exit_code, output, err, _command, start_time, end_time, timed_out) = run_command(cmd, directory=work_directory, env=env, proc=proc, verbose=False)
            log.info("Start*: %s", start_time.strftime('%Y-%m-%dT%H:%M:%S'))
            log.info("End:    %s", datetime.now().strftime('%Y-%m-%dT%H:%M:%S'))
            log.info("End*:   %s", end_time.strftime('%Y-%m-%dT%H:%M:%S'))
        except OSError as e:  # pragma: no cover
            log.error("Error running: %s, %s", test_name, e)
            exit_code = 1
            output = ''
            err = str(e)
            start_time = datetime.now()
            end_time = datetime.now()
            timed_out = False

        stdout = os.path.join(work_directory, "stdout.txt")
        stderr = os.path.join(work_directory, "stderr.txt")
        with open(stdout, "w", encoding='UTF-8') as text_file:
            log.debug("Copying stdout into %s", stdout)
            text_file.write(output)
        with open(stderr, "w", encoding='UTF-8') as text_file:
            log.debug("Copying stderr into %s", stderr)
            text_file.write(err)

        # extras
        result = ArtBase.get_art_results(output)

        log.info("Job ended %d %s %s %s %s %s %d", exit_code, sequence_tag, package, job_type, test_directory, test_name, job_index)
        log.info("Exit Code: %d, timed out: %r", exit_code, timed_out)

        copy_exit_code = 0
        if copy:
            srcdir = work_directory
            tstdir = os.path.join(copy, package)
            tar_file = work_directory + ".tar.gz"
            log.info("Copying results for %s", base_test_name)
            log.info("Creating and copying tar from %s", srcdir)
            tar(tar_file, srcdir)
            copy_exit_code |= xrdcp(tar_file, tstdir)

            # NOTE no longer copy separate files
            # log.info("Source: %s", srcdir)
            # log.info("Target: %s", tstdir)
            # # NOTE: first copy the srcdir, as this copied a full directory and creates base_test_name,
            # # then copy the two std files
            # copy_exit_code |= xrdcp(srcdir, tstdir)
            # # copy_exit_code |= xrdcp(stdout, os.path.join(tstdir, base_test_name))
            # # copy_exit_code |= xrdcp(stderr, os.path.join(tstdir, base_test_name))

            log.info("Copy Exit Code: %d", copy_exit_code)

        return (test_name, exit_code | copy_exit_code, start_time, end_time, test_directory, description, result)
