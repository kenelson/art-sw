#!/bin/sh
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration

set -e

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh || echo $? && true
NIGHTLY_RELEASE=$2
NIGHTLY_RELEASE_SHORT=${NIGHTLY_RELEASE/-VAL-*/-VAL}
export RUCIO_ACCOUNT=artprod
lsetup panda || echo $? && true
echo "Running: asetup --platform=$4 ${NIGHTLY_RELEASE_SHORT},$5,$3"
asetup --platform=$4 ${NIGHTLY_RELEASE_SHORT},$5,$3 || echo $? && true
lsetup -f "rucio -w" -a artprod || echo $? && true
# voms-proxy-init --rfc -noregen -cert ./grid.proxy -voms atlas
voms-proxy-init --rfc -noregen -cert ./grid.proxy -voms atlas:/atlas/art/Role=production --valid 48:00

lsetup -f "xrootd 5.1.1" || echo $? && true
which xrdcp

source share/localSetupART.sh
source test/localTestSetup.sh

echo "Running UnitTests"
echo "Running Local Tests for python2"
python --version
python -m unittest -v $1
