#!/bin/zsh
# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
SOURCE=${(%):-%N}
SCRIPTPATH="$( cd "$( dirname "$SOURCE" )" && pwd )"
export PATH=${SCRIPTPATH}/../scripts:${PATH}
export PYTHONPATH=${SCRIPTPATH}/../python:${PYTHONPATH}
